import 'package:flutter/material.dart';
import 'package:pet_services_flutter/authenticate/authenticate.dart';
import 'package:pet_services_flutter/models/pet_owner.dart';
import 'package:pet_services_flutter/screens/home.dart';
import 'package:provider/provider.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // This line of code calls the provider and specify what type of data the stream will get
    // Provider needs to have the context as a parameter
    final petOwner= Provider.of<PetOwner>(context);

  //Check if the user is loged in depending on show a screen

    if (petOwner ==null){
      print('Redirecting to Auth');
      return Authenticate();
    }else{
      petOwner.theme = ThemeMode.light;
      print('Redirecting to home');
      return HomeScreen();
    }
  }
}
