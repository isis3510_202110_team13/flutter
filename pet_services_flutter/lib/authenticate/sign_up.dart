import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pet_services_flutter/services/auth.dart';
import 'package:pet_services_flutter/services/connection_status_singleton.dart';
import 'package:pet_services_flutter/services/pet_owners_service.dart';

class SignUp extends StatefulWidget {
  final Function toggleView;

  SignUp({this.toggleView});

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {

  StreamSubscription _connectionChangeStream;
  bool isOffline = true;

  final AuthServiceF _auth = AuthServiceF();
  final PetOwnersService _ownersService=PetOwnersService();
  // Global key to keep track of the state of the form
  final _formKey= GlobalKey<FormState>();

  // Text field state
  String displayName = '';
  String email = '';
  String password = '';
  String error='';

  void connectionChanged(dynamic hasConnection) {
    setState(() {
      isOffline = !hasConnection;
    });
  }

  @override
  void initState() {
    super.initState();

    ConnectionStatusSingleton connectionStatus =
    ConnectionStatusSingleton.getInstance();
    ConnectionStatusSingleton.getInstance().checkConnection().then((value) => {
      setState(() {
        isOffline = !value;
      })
    });
    _connectionChangeStream =
        connectionStatus.connectionChange.listen(connectionChanged);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text('Sign Up'),
        actions: <Widget>[
          ElevatedButton.icon(
              onPressed: () =>{
                widget.toggleView()
              },
              icon: Icon(Icons.person),
              label: Text('Sign In '),
              style: ElevatedButton.styleFrom(primary: Colors.green))
        ],
      ),

      //Form container
      body: Container(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
          child: Form(
           key:_formKey,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  // Display name field
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(

                    onChanged: (val) {
                      setState(() {displayName = val;});
                    },
                    decoration: InputDecoration(
                        labelText: 'Name'
                    ),

                  ),
                  // email field
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    validator: (String val) {
                      return (val.isEmpty || !val.contains('@')) ? 'Email is not valid' : null;
                    },
                    onChanged: (val) {
                      setState(() {email = val;});
                    },
                    decoration: InputDecoration(
                      labelText: 'Email address'
                    ),
                  ),
                  // password field
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    validator:(val)=> val.length<8? "Password must have at least 8 characters":null,
                    onChanged: (val) {
                      setState(() {password = val;});
                    },
                    decoration: InputDecoration(
                        labelText: 'Password'
                    ),
                    obscureText: true,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  // Sign In button
                  ElevatedButton(
                      onPressed: () async {
                        if (_formKey.currentState.validate()){
                          dynamic result = await _auth.registerWithEmailAndPassword(email, password);
                          if (result== null){
                            setState(() {
                              error="couldn't register";
                            });
                          }
                          else{
                            try{
                              dynamic user_creation= await _ownersService.createPetOwner(result.uid, displayName);
                              print("User creation "+ user_creation);
                            }catch(e){
                              print(e);
                            }

                          }
                        }
                      },
                      child: Text('Register'),
                      style: ElevatedButton.styleFrom(
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0),
                          ),
                          primary: Colors.green)),
                ],
              ),
            ),
          )),
    );
  }
}
