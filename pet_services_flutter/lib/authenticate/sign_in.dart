import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pet_services_flutter/screens/no_conection_bar.dart';
import 'package:pet_services_flutter/services/auth.dart';
import 'package:pet_services_flutter/services/connection_status_singleton.dart';
import 'package:pet_services_flutter/widgets/alert.dart';

class SignIn extends StatefulWidget {
  final Function toggleView;

  SignIn({this.toggleView});

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final AuthServiceF _auth = AuthServiceF();

  // Global key to keep track of the state of the form
  final _formKey = GlobalKey<FormState>();


  StreamSubscription _connectionChangeStream;
  bool isOffline = true;

  // Text field state
  String email = '';
  String password = '';
  String error='';
  void connectionChanged(dynamic hasConnection) {
    setState(() {
      isOffline = !hasConnection;
    });
  }

  @override
  void initState() {
    super.initState();

    ConnectionStatusSingleton connectionStatus =
    ConnectionStatusSingleton.getInstance();
    ConnectionStatusSingleton.getInstance().checkConnection().then((value) => {
      setState(() {
        isOffline = !value;
      })
    });
    _connectionChangeStream =
        connectionStatus.connectionChange.listen(connectionChanged);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text('Sign In'),
          actions: <Widget>[
            ElevatedButton.icon(
                onPressed: () => {widget.toggleView()},
                icon: Icon(Icons.person),
                label: Text('Register'),
                style: ElevatedButton.styleFrom(primary: Colors.green))
          ]),
      body:
      Column(
        children: [
          isOffline ? NoConectionBar() : Container(),
          Container(
              padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    // email field
                    SizedBox(
                      height: 20.0,
                    ),
                    TextFormField(
                      validator: (String value) {
                        return (value == null && !value.contains('@'))
                            ? 'Email is not valid'
                            : null;
                      },
                      onChanged: (val) {
                        setState(() {
                          email = val;
                        });
                      },
                      decoration: InputDecoration(labelText: 'Email address'),
                    ),
                    // password field
                    SizedBox(
                      height: 20.0,
                    ),
                    TextFormField(
                      onChanged: (val) {
                        setState(() {
                          password = val;
                        });
                      },
                      decoration: InputDecoration(labelText: 'Password'),
                      obscureText: true,
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    // Sign In button
                    ElevatedButton(
                        onPressed: () async {
                          if(isOffline){
                            AuthyAlert.showErrorDialog(
                                context, 'No internet conection');
                          }
                          else{
                            if (_formKey.currentState.validate()) {
                              dynamic result = await _auth
                                  .signInWithEmailAndPassword(email, password);
                              if (result == null) {
                                setState(() {
                                  error = "couldn't Sign in";
                                });
                              }
                            }
                          }


                        },
                        child: Text('Sign In'),
                        style: ElevatedButton.styleFrom(
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0),
                            ),
                            primary: isOffline?Colors.blueGrey:Colors.green)),
                  ],
                ),
              )),
        ],
      )

    );
  }
}
