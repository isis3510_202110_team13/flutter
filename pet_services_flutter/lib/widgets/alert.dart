import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class AuthyAlert {
  static Future<void> showErrorDialog(
      BuildContext context, String errorMessage) async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: Text('Error', style: TextStyle(color: Colors.red)),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Text(errorMessage, style: TextStyle(color: Colors.black))
                ],
              ),
            ),
            actions: [
              FlatButton(
                child: Text(
                  'Ok',
                  style: TextStyle(color: Colors.black),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  static Future<void> showInfoDialog(
      BuildContext context,String title, String message) async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: Text(title, style: TextStyle(color: Colors.blue)),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Text(message, style: TextStyle(color: Colors.black))
                ],
              ),
            ),
            actions: [
              FlatButton(
                child: Text(
                  'Ok',
                  style: TextStyle(color: Colors.black),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }


  static Future<void> showCodeConfirmationDialog(
      BuildContext context) async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: Text('Verify Phone Number',
                style: TextStyle(color: Colors.white)),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Text(
                    'A confirmation code has been sent to your phone, enter to continue',
                    style: TextStyle(color: Colors.white),
                  ),
                  StreamBuilder<String>(
                      builder: (context, snapshot) {
                        return null;
                      })
                ],
              ),
            ),
            actions: [
              FlatButton(
                child: Text(
                  'Cancel',
                  style: TextStyle(color: Colors.deepPurple),
                ),
                onPressed: () => Navigator.of(context).pop(),
              ),
              FlatButton(
                child: Text(
                  'Submit',
                  style: TextStyle(color: Colors.deepPurple),
                ),

              )
            ],
          );
        });
  }

  static Future<void> showAutomaticConfirmationDialog(
      BuildContext context) async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: Text('Verified', style: TextStyle(color: Colors.white)),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Text(
                      'Your phone is equipped with automated verification, you may ignore the verification text',
                      style: TextStyle(color: Colors.white))
                ],
              ),
            ),
            actions: [
              FlatButton(
                child: Text(
                  'Okay',
                  style: TextStyle(color: Colors.deepPurple),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  static Future<void> showEmailVerifyNotice(
      BuildContext context, String email) async {
    //Default Value if no email submitted
    if (email == null) email = 'your email address';

    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: Text('Verified', style: TextStyle(color: Colors.white)),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  Text(
                      'We\'ve sent a message to $email.  Please open to verify your email address',
                      style: TextStyle(color: Colors.white))
                ],
              ),
            ),
            actions: [
              FlatButton(
                child: Text(
                  'Okay',
                  style: TextStyle(color: Colors.deepPurple),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }
}