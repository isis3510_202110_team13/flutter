import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:pet_services_flutter/models/pet_owner.dart';
import 'package:pet_services_flutter/services/auth.dart';
import 'package:pet_services_flutter/wrapper.dart';
import 'package:provider/provider.dart';
import 'package:pet_services_flutter/services/connection_status_singleton.dart';
import 'package:firebase_performance/firebase_performance.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  ConnectionStatusSingleton connectionStatus = ConnectionStatusSingleton.getInstance();
  connectionStatus.initialize();
  FirebaseAnalytics().logAppOpen();
  final Trace trace = FirebasePerformance.instance.newTrace("Initialize");
  await Firebase.initializeApp();
  trace.stop();
  FirebaseCrashlytics.instance
      .setCrashlyticsCollectionEnabled(true);
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<PetOwner>.value(
      //Especifies what stream the provider is going to listen
      value: AuthServiceF().petOwner,
      child: MaterialApp(
        home: Wrapper(),
      ),
    );
  }
}

/*
void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  ConnectionStatusSingleton connectionStatus = ConnectionStatusSingleton.getInstance();
  connectionStatus.initialize();
  await Firebase.initializeApp();
  runApp(MyHomePage());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.lightGreen,
      ),
      home: MyHomePage(),
      //MyHomePage(title: 'Pet Services'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  //final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  final authBloc = AuthBloc();
  @override
  Widget build(BuildContext context) {
    debugPrint("$AuthService.user()");
    if (AuthService().user()==null)
      return Provider(
      create: (context) => authBloc,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
            visualDensity: VisualDensity.adaptivePlatformDensity,
            ),
        home: LoginScreen()
        ),
      );
    else
      return Provider(
        create: (context) => authBloc,
        child: MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
              visualDensity: VisualDensity.adaptivePlatformDensity,
            ),
            home: HomeScreen()
        ),
      );
  }
}
*/









//   Scaffold(
//   appBar: AppBar(
//     // Here we take the value from the MyHomePage object that was created by
//     // the App.build method, and use it to set our appbar title.
//     title: Text(widget.title),
//   ),
//   body: Center(
//     // Center is a layout widget. It takes a single child and positions it
//     // in the middle of the parent.
//     child: Column(
//       // Column is also a layout widget. It takes a list of children and
//       // arranges them vertically. By default, it sizes itself to fit its
//       // children horizontally, and tries to be as tall as its parent.
//       //
//       // Invoke "debug painting" (press "p" in the console, choose the
//       // "Toggle Debug Paint" action from the Flutter Inspector in Android
//       // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
//       // to see the wireframe for each widget.
//       //
//       // Column has various properties to control how it sizes itself and
//       // how it positions its children. Here we use mainAxisAlignment to
//       // center the children vertically; the main axis here is the vertical
//       // axis because Columns are vertical (the cross axis would be
//       // horizontal).
//       mainAxisAlignment: MainAxisAlignment.center,
//       children: <Widget>[
//         Text(
//           'You have pushed the button this many times:',
//         ),
//         Text(
//           '$_counter',
//           style: Theme.of(context).textTheme.headline4,
//         ),
//         UnStateFull(),
//       ],
//     ),
//   ),
//   floatingActionButton: FloatingActionButton(
//     onPressed: _incrementCounter,
//     tooltip: 'Increment',
//     child: Icon(Icons.add),
//   ), // This trailing comma makes auto-formatting nicer for build methods.
// );
