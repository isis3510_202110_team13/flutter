import 'dart:convert';
import 'dart:io';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:http/http.dart' as http;

import 'dart:async';
import 'package:http/http.dart';
import 'package:pet_services_flutter/models/pet_owner.dart';
class PetOwnersService{

  final URL = 'https://secure-island-73617.herokuapp.com/petowners';


  PetOwnersService(){
  }

  Future<Response> getPetOwners() {
    FirebaseAnalytics().logEvent(name: "get_pet_owners");
    return http.get(Uri.parse("$URL"));

  }
  Future<http.Response> getPetOwner( String id) {
    FirebaseAnalytics().logEvent(name: "get_pet_owner");
    return http.get(Uri.parse("$URL/$id"));
  }

  Future<http.Response> updateUser( String id, user) {
    FirebaseAnalytics().logEvent(name: "update_user");
    Map<String, String> headers = {HttpHeaders.contentTypeHeader: "application/json"};
    return http.put(Uri.parse("$URL/$id/users"),headers: headers,body:jsonEncode(user));
  }

  Future<http.Response> getPets( String id) {
    getPetsOffline(id);
    FirebaseAnalytics().logEvent(name: "get_pets");
    return http.get(Uri.parse("$URL/$id/pets"));
  }
  Future<File> getPetsOffline( String id) {
    return DefaultCacheManager().getSingleFile("$URL/$id/pets");
  }

  Future<http.Response> postPet( String id, pet) {
    FirebaseAnalytics().logEvent(name: "create_pet");
    Map<String, String> headers = {HttpHeaders.contentTypeHeader: "application/json"};
    return http.post(Uri.parse("$URL/$id/pets"),headers: headers,body:jsonEncode(pet));
  }


  Future<http.Response> createPetOwner(String uid, String displayName) async {
    FirebaseAnalytics().logEvent(name: "create_pet_owner");
    PetOwner petOwner = PetOwner(
        uid: uid,
        displayName: displayName,
        photoUrl: null,
        coordType: 'Point',
        latitude: -74.6,
        longitude: 4.6);
    print(jsonEncode(petOwner));
    return await http.post(
      Uri.https('secure-island-73617.herokuapp.com', '/petOwners'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(petOwner),
    );
  }

}
