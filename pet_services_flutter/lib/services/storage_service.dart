import 'package:firebase_storage/firebase_storage.dart';
class StorageService{
  final FirebaseStorage storage = FirebaseStorage.instance;

  Future<void> uploadPetFile(name,file) async {
    //UploadTask task = storage.ref('pets/${name}}').putFile(file);
    try {
      await storage
          .ref('pets/${name}')
          .putFile(file);
    } on FirebaseException catch (e) {
      // e.g, e.code == 'canceled'

    }
  }

  Future<void> uploadUserFile(name,file) async {
    //UploadTask task = storage.ref('users/${name}}').putFile(file);
    try {
      await storage
          .ref('users/${name}')
          .putFile(file);
    } on FirebaseException catch (e) {
      // e.g, e.code == 'canceled'

    }
  }

  Future<String> getURL(name) async {
    return storage.ref('pets/${name}').getDownloadURL();
  }

  Future<String> getURLUser(name) async {
    return storage.ref('users/${name}').getDownloadURL();
  }

}




