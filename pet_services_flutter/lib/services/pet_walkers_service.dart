
import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:http/http.dart' as http;

import 'dart:async';

import 'package:http/http.dart';
class PetWalkersService{

  final server = 'secure-island-73617.herokuapp.com';
  final resource = '/petwalkers';
  final URL = 'https://secure-island-73617.herokuapp.com/petwalkers';

  PetWalkersService(){
  }


  Future<Response> getPetWalkers({String uid='',bool matched=false}) {
    getPetWalkersOffline(uid,matched);
    FirebaseAnalytics().logEvent(name: "get_pet_walkers");
    if(matched){
      Map<String, String> queryParameters = {'petOwnerUid':uid,'matched':"$matched"};
      return http.get(Uri.https(server, resource,queryParameters) );
    }
    else{
      return http.get(Uri.https(server, resource) );
    }

  }
  Future<File> getPetWalkersOffline(String uid,bool matched) {
    Map<String, String> queryParameters = {'petOwnerUid':uid,'matched':"$matched"};
    return DefaultCacheManager().getSingleFile(Uri.https(server, resource,queryParameters).toString());

  }

  Future<Response> getTopPetWalkers(String uid,bool matched) {
    getTopPetWalkersOffline(uid,matched);
    Map<String, String> queryParameters = {'petOwnerUid':uid,'orderBy': 'avgRating','matched':"$matched"};
    FirebaseAnalytics().logEvent(name: "get_top_pet_walkers");
    return http.get(Uri.https(server, resource,queryParameters) );

  }
  Future<File> getTopPetWalkersOffline(String uid,bool matched) {
    Map<String, String> queryParameters = {'petOwnerUid':uid,'orderBy': 'avgRating','matched':"$matched"};
    return DefaultCacheManager().getSingleFile(Uri.https(server, resource,queryParameters).toString());
  }

  Future<Response> getNearPetWalkers(String uid,bool matched, double lat, double long) {
    Map<String, String> queryParameters = {'petOwnerUid':uid,'orderBy': 'distance','matched':"$matched",'lat':"$lat",'long':"$long"};
    FirebaseAnalytics().logEvent(name: "get_near_pet_walkers");
    return http.get(Uri.https(server, resource,queryParameters) );

  }
  // Future<File> getNearPetWalkersOffline() {
  //   return DefaultCacheManager().getSingleFile(URL);
  // }



  Future<http.Response> getPetWalker( String id) {
    getPetWalkerOffline(id);
    FirebaseAnalytics().logEvent(name: "get_pet_walker");
    return http.get(Uri.parse("$URL/$id"));
  }
  Future<File> getPetWalkerOffline( String id) {
    return DefaultCacheManager().getSingleFile("$URL/$id");
  }


}