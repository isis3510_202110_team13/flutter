import 'dart:convert';
import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:http/http.dart' as http;

import 'dart:async';

import 'package:http/http.dart';
class WalksService{

  final server = 'secure-island-73617.herokuapp.com';
  final resource = '/petwalkers';
  final URL = 'https://secure-island-73617.herokuapp.com/petwalkers';

  WalksService(){

  }

  Future<http.Response> postWalk( walk) {
    FirebaseAnalytics().logEvent(name: "create_pet");
    Map<String, String> headers = {HttpHeaders.contentTypeHeader: "application/json"};
    return http.post(Uri.parse("$URL/walks"),headers: headers,body:jsonEncode(walk));
  }
}