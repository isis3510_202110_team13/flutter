import 'package:flutter/material.dart';
import 'package:pet_services_flutter/services/auth.dart';

class Home extends StatelessWidget {

  final AuthServiceF _auth= AuthServiceF();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pet sevices'),
        elevation: 0.0,
        actions: <Widget>[
          ElevatedButton.icon(
              onPressed: () async{
                await _auth.signOut();
              },
              icon: Icon(Icons.person),
              label: Text('Log out'))
        ],
      ),
    );
  }
}
