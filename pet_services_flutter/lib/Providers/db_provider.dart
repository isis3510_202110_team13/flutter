import 'dart:io';
import 'package:path/path.dart';

import 'package:path_provider/path_provider.dart';
import 'package:pet_services_flutter/models/pet_walker.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  static Database _database;
  static final DBProvider db = DBProvider._();

  DBProvider._();

  Future<Database> get database async {
    // If database exists, return database
    if (_database != null){
      print("La base de datos ya esta creada :D");
      return _database;
    }

    // If database don't exists, create one
    _database = await initDB();

    return _database;
  }

  // Create the database and the Employee table
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'pet_walker_manager.db');
    print("databasepath:-------------------------- "+path);
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE PetWalker('
          'uid INTEGER PRIMARY KEY,'
          'displayName TEXT,'
          'photoURL TEXT,'
          'description TEXT,'
          'avgRating REAL'
          ')');
    });
  }

  // Insert petWalker on database
  createPetWalker(PetWalker newPetWalker) async {
    print("Entramos a create pet walker");
    await deleteAllPetWalkers();
    final db = await database;
    final res = await db.insert('PetWalker', newPetWalker.toJson());

    return res;
  }

  // Delete all petWalkers
  Future<int> deleteAllPetWalkers() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM PetWalker');

    return res;
  }

  Future<List<PetWalker>> getAllPetWalkers() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM PETWALKER");

    List<PetWalker> list =
        res.isNotEmpty ? res.map((c) => PetWalker.fromJson(c)).toList() : [];

    return list;
  }
}