import 'package:flutter/material.dart';
import 'package:pet_services_flutter/screens/pets_list.dart';
import 'package:pet_services_flutter/screens/top_rated_walkers.dart';
import 'package:geolocator/geolocator.dart';
import 'package:pet_services_flutter/screens/moreScreen.dart';

import 'request_walk.dart';

class HomeScreenBody extends StatefulWidget {
  @override
  _HomeScreenBodyState createState() => _HomeScreenBodyState();
}

class _HomeScreenBodyState extends State<HomeScreenBody> {
  _checkPermissions() async {}

  var locationMessage = "";
  var lat;
  var long;
  Geolocator _geolocator=Geolocator();
  // Future _getCurrentLocation2() async {
  //   print("Hello de geolocator");
  //   var position = await _geolocator.getCurrentPosition(
  //       desiredAccuracy: LocationAccuracy.high);
  //   var lastPosition = await _geolocator.getLastKnownPosition();
  //   lat = lastPosition.latitude;
  //   long = lastPosition.longitude;
  //   lat = position.latitude;
  //   long = position.longitude;
  //   print(lat);
  //   print(long);
  // }
  Future _getCurrentLocation() async {
    _geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) => {
              setState(() {
                locationMessage = "$position.latitude, $position.longitude";
              })
            })
        .catchError((e) {
      print(e);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
        title: Text('Home'),
        backgroundColor: Colors.green[900],
        ),
        body:Container(
            child:
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image(
                      image: AssetImage("assets/logo.png"),
                      height: 200,
                    )

                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      children: <Widget>[
                        RichText(
                            text: TextSpan(
                                text:"Pet services",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 18,
                                )
                            ),
                        ),

                      ],
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[

                    Column(
                      children: <Widget>[
                        ElevatedButton(
                          child: RichText(
                            text: TextSpan(
                              text:"Top rated PW",
                              style:TextStyle(
                                color: Colors.white,
                                fontSize: 15.0,
                              ),
                            ),
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: Colors.green[900],
                          ),
                          onPressed: () => Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                  builder: (context) => TopRatedWalkers())),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        ElevatedButton(
                          child: RichText(
                            text: TextSpan(
                              text:"My pets",
                              style:TextStyle(
                                color: Colors.white,
                                fontSize: 15.0,
                              ),
                            ),
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: Colors.green[900],
                          ),
                          onPressed: () => Navigator.of(context).pushReplacement(
                              MaterialPageRoute(builder: (context) => PetsList())),
                        ),
                      ],
                    ),
                  ],
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          ElevatedButton(
                            child: RichText(
                              text: TextSpan(
                                text:"Request walk",
                                style:TextStyle(
                                  color: Colors.white,
                                  fontSize: 15.0,
                                ),
                              ),
                            ),
                            style: ElevatedButton.styleFrom(
                              primary: Colors.green[900],
                            ),
                            onPressed: () => Navigator.of(context).pushReplacement(
                                MaterialPageRoute(builder: (context) => TopRatedWalkers())),
                          ),
                        ],
                      )
                    ]
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          ElevatedButton(
                            child: RichText(
                              text: TextSpan(
                                text:"More",
                                style:TextStyle(
                                  color: Colors.white,
                                  fontSize: 15.0,
                                ),
                              ),
                            ),
                            style: ElevatedButton.styleFrom(
                              primary: Colors.green[900],
                            ),
                            onPressed: () => Navigator.of(context).pushReplacement(
                                MaterialPageRoute(builder: (context) => MoreScreen())),
                          ),
                        ],
                      ),
                    ]
                ),
              ],
            ),
        )
    );
  }

  Future<Map<String, double>> _getLocation() async {
    var currentLocation = <String, double>{};
    try {} catch (e) {
      currentLocation = null;
    }
    return currentLocation;
  }
}
