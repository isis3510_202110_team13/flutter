import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NoConectionBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.brown,
      width: double.infinity,
      height: 30,
      child: Padding(
          padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
          child:
          Text('No internet conection, showing previous data',
              style: TextStyle(
                backgroundColor: Colors.brown,
                color: Colors.white,
              ))),
    );
  }
}

