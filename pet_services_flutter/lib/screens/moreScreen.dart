import 'package:flutter/material.dart';
import 'package:pet_services_flutter/services/auth.dart';
import 'package:pet_services_flutter/screens/homeScreenBody.dart';
import 'package:pet_services_flutter/widgets/alert.dart';

class MoreScreen extends StatefulWidget {
  @override
  _MoreScreenState createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {

  bool isOffline = false;

  var petWalkers = [];
  var uid='';
  var insuranceCompanies =[];

  final AuthServiceF _auth= AuthServiceF();

  void connectionChanged(dynamic hasConnection) {
    setState(() {
      isOffline = !hasConnection;
    });
  }

  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('More'),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            size: 45.0,
            color: Colors.white,
          ),
          onPressed: () => Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomeScreenBody())),
        ),
        backgroundColor: Colors.green[900],
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ClipOval(
                  child: Material(
                    color: Colors.red, // button color
                    child: InkWell(
                      splashColor: Colors.green[900], // inkwell color
                      child: SizedBox(width: 200, height: 200,
                          child: Center(
                            child: RichText(
                              text: TextSpan(
                                text:"HELP",
                                style:TextStyle(
                                  color: Colors.white,
                                  fontSize: 25.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                      ),
                      onTap: () {},
                        onLongPress:(){
                          AuthyAlert.showInfoDialog(
                              context, "Emergencia registrada", "Recibiras ayuda pronto");
                        },
                    ),
                  ),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children:<Widget>[
                Text('Insurance',
                    style: TextStyle(
                        color: Colors.green,
                        fontSize: 22.0,
                        fontWeight: FontWeight.w600)
                ),
                /*ListView.builder(
                  itemCount: insuranceCompanies == null ? 0 : insuranceCompanies.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () => Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                              builder: (context) => InsuranceDetail(
                                id: "${insuranceCompanies[index]['id']}",
                                name:insuranceCompanies[index]['name'],
                                cost:insuranceCompanies[index]['size'],
                                photoURL: insuranceCompanies[index]['photoURL'],
                              )
                          ),
                      ),
                      child: Card(
                        child: Row(
                          children: <Widget>[
                            insuranceCompanies[index]['photoURL'] != ''
                                ? Image.network(
                                insuranceCompanies[index]['photoURL'],
                                height: 100,
                                width: 100)
                                : Image.asset('assets/dogWalker.png',
                                height: 100, width: 100),
                            Padding(
                                padding: const EdgeInsets.all(30.0),
                                child: Text(
                                  "${insuranceCompanies[index]["name"]}",
                                  style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                            ),
                            Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Text(
                                  "Cost: ${insuranceCompanies[index]["Cost"]}",
                                  style: TextStyle(
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.green[900]),
                                )),
                          ],
                        ),
                      ),
                    );
                  },
                ),*/
              ],
            ),
          ],
        ),
      ),
    );

  }
}