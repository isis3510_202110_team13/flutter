
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pet_services_flutter/screens/top_rated_walkers.dart';
import 'package:pet_services_flutter/services/pet_walkers_service.dart';
import 'package:pet_services_flutter/screens/pet_walker_detail.dart';
import 'package:pet_services_flutter/services/connection_status_singleton.dart';
import 'dart:async';
import 'dart:convert';
import 'no_conection_bar.dart';

class PetWalkersMap extends StatefulWidget {
  @override
  _PetWalkersMapState createState() => _PetWalkersMapState();
}

class _PetWalkersMapState extends State<PetWalkersMap> {
  StreamSubscription _connectionChangeStream;
  _checkPermissions() async {}

  var locationMessage = "";
  var orderType="";
  double lat;
  double long;
  Geolocator _geolocator=Geolocator();
  Future<Position> _getCurrentLocation() async {
    return _geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best);}

  bool isOffline = true;
  var petWalkers = [];

  void connectionChanged(dynamic hasConnection) {
    setState(() {
      isOffline = !hasConnection;
    });
  }

  Future<void> _onMapCreated(GoogleMapController controller) async {}

  @override
  void initState() {
    super.initState();

    ConnectionStatusSingleton connectionStatus =
    ConnectionStatusSingleton.getInstance();
    ConnectionStatusSingleton.getInstance().checkConnection().then((value) => {
      //debugPrint("${value}")
      setState(() {
        isOffline = !value;
      })
    });
    _connectionChangeStream =
        connectionStatus.connectionChange.listen(connectionChanged);
    // PetWalkersService().getTopPetWalkers().then((Response response) {
    //   petWalkers = jsonDecode(response.body);
    //
    //   debugPrint("${petWalkers}");
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Pet walkers'),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_outlined,
              size: 45.0,
              color: Colors.white,
            ),
            onPressed: () => Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => TopRatedWalkers())),
          ),
          backgroundColor: Colors.green[900],
        ),

        body:
           isOffline? NoConectionBar():

               FutureBuilder(
                   future: _getCurrentLocation(),
                   builder: (context,snapshot){
                     if(snapshot.connectionState==ConnectionState.done){
                       lat= snapshot.data.latitude;
                       long= snapshot.data.longitude;
                       return

                         FutureBuilder(
                             future: PetWalkersService().getTopPetWalkers('',false),
                             builder: (context,snapshot2){
                               if(snapshot2.connectionState==ConnectionState.done){

                                 final Map<String, Marker> _markers = {};
                                 var dogWalkers=jsonDecode(snapshot2.data.body);
                                 debugPrint("$dogWalkers");

                                 dogWalkers.forEach((dw) async {
                                   final marker = Marker(
                                     markerId: MarkerId(dw["uid"]),
                                     //icon: await BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(48, 48)),"assets/dogWalker.png"),
                                     position: LatLng(dw["currentLocation"]["coordinates"][1], dw["currentLocation"]["coordinates"][0]),
                                     infoWindow: InfoWindow(
                                       title: dw["displayName"],
                                       snippet: dw["city"],
                                         onTap: () {
                                           debugPrint("${dw["uid"]}");
                                           Navigator.of(context)
                                               .pushReplacement(MaterialPageRoute(
                                               builder: (context) => PetWalkerDetail(
                                                   id: dw['uid'])));
                                         }
                                     ),


                                   );
                                   _markers[dw["uid"]] = marker;

                                   //debugPrint("${dw["currentLocation"]["coordinates"]}");
                                 });

                                 debugPrint("${_markers}");
                                 return GoogleMap(
                                   onMapCreated: _onMapCreated,
                                   myLocationEnabled: true,
                                   initialCameraPosition: CameraPosition(
                                     target:  LatLng(lat, long),
                                     zoom: 11,
                                   ),
                                   markers: _markers.values.toSet(),
                                 );
                               }
                               else{
                                 return CircularProgressIndicator();
                               }
                             });



                     }
                     else{
                       return CircularProgressIndicator();
                     }

                   })





    );
  }
}

  