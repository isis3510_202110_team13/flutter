
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:pet_services_flutter/screens/pets_list.dart';
import 'package:pet_services_flutter/services/connection_status_singleton.dart';
import 'dart:async';
import 'package:pet_services_flutter/widgets/alert.dart';

import 'no_conection_bar.dart';

class PetDetail extends StatefulWidget {
  final String id;
  final String name;
  final String breed;
  final String photoURL;
  final String age;
  final String size;

  PetDetail(
      {this.id, this.name, this.breed, this.age, this.size, this.photoURL});

  @override
  _PetDetailState createState() => _PetDetailState();
}

class _PetDetailState extends State<PetDetail> {
  StreamSubscription _connectionChangeStream;
  bool isOffline = true;
  var petWalkers = [];
  var petWalker;

  void connectionChanged(dynamic hasConnection) {
    setState(() {
      isOffline = !hasConnection;
    });
  }

  @override
  void initState() {
    super.initState();

    ConnectionStatusSingleton connectionStatus =
        ConnectionStatusSingleton.getInstance();
    ConnectionStatusSingleton.getInstance().checkConnection().then((value) => {
      setState(() {
        isOffline = !value;
      })
    });
    _connectionChangeStream =
        connectionStatus.connectionChange.listen(connectionChanged);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.name),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_outlined,
              size: 45.0,
              color: Colors.white,
            ),
            onPressed: () => Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => PetsList())),
          ),
          backgroundColor: Colors.green[900],
        ),
        body: SingleChildScrollView(
            child: Column(
          children: <Widget>[
            isOffline ? NoConectionBar() : Container(),
            Column(// here only return is missing
                children: [
              Container(
                  width: double.infinity,
                  height: 150,
                  child: Container(
                      alignment: Alignment(0.0, 2.5),
                      child: widget.photoURL == null
                          ? CircleAvatar(
                              backgroundImage:
                                  AssetImage('assets/dogWalker.png'),
                              radius: 60.0,
                            )
                          : CircleAvatar(
                              backgroundImage: NetworkImage(widget.photoURL),
                              radius: 60.0,
                            ))),
              SizedBox(
                height: 50,
              ),
              Text(widget.name,
                  style: TextStyle(
                      fontSize: 25.0,
                      color: Colors.blueGrey,
                      letterSpacing: 2.0,
                      fontWeight: FontWeight.w400)),
              SizedBox(
                height: 10,
              ),
              Text(widget.breed,
                  style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.black45,
                      letterSpacing: 2.0,
                      fontWeight: FontWeight.w300)),
              SizedBox(
                height: 20,
              ),
              RaisedButton(
                onPressed: () {
                  isOffline
                      ? AuthyAlert.showErrorDialog(
                          context, 'No internet conection')
                      :

                  FirebaseCrashlytics.instance.crash();
                  // Navigator.of(context).pushReplacement(MaterialPageRoute(
                  //     builder: (context) => HomeScreen()));
                  //

                },
                color: isOffline ? Colors.blueGrey : Colors.green,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(80.0),
                ),
                child: Ink(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(80.0),
                  ),
                  child: Container(
                    constraints: BoxConstraints(
                      maxWidth: 100.0,
                      maxHeight: 40.0,
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      "Do something",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 12.0,
                          letterSpacing: 2.0,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                ),
              ),
              Card(
                  margin:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 30.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                                child: Column(children: [
                              Text('Basic information',
                                  style: TextStyle(
                                      color: Colors.green,
                                      fontSize: 22.0,
                                      fontWeight: FontWeight.w600)),
                              SizedBox(
                                height: 30,
                              ),
                              Row(
                                children: [
                                  Padding(
                                      padding:
                                          const EdgeInsets.only(right: 100.0),
                                      child: Text('Age',
                                          style: TextStyle(
                                              color: Colors.brown,
                                              fontSize: 19.0,
                                              fontWeight: FontWeight.w600))),
                                  Text(widget.age)
                                ],
                              ),
                              Row(
                                children: [
                                  Padding(
                                      padding:
                                          const EdgeInsets.only(right: 100.0),
                                      child: Text('Size',
                                          style: TextStyle(
                                              color: Colors.brown,
                                              fontSize: 19.0,
                                              fontWeight: FontWeight.w600))),
                                  Text(widget.size)
                                ],
                              ),
                            ])))
                      ])),
            ])
          ],
        )));
  }
}
