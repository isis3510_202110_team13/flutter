import 'dart:io';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:pet_services_flutter/models/pet_owner.dart';
import 'package:pet_services_flutter/screens/PetWalkersMap.dart';
import 'package:pet_services_flutter/screens/homeScreenBody.dart';
import 'package:pet_services_flutter/screens/pets_list.dart';
import 'package:pet_services_flutter/services/pet_walkers_service.dart';
import 'package:pet_services_flutter/screens/pet_walker_detail.dart';
import 'package:pet_services_flutter/services/connection_status_singleton.dart';
import 'dart:async';
import 'dart:convert';
import 'no_conection_bar.dart';
import 'package:provider/provider.dart';


enum Filters{none, rating, distance}

class TopRatedWalkers extends StatefulWidget {
  @override
  _TopRatedWalkersState createState() => _TopRatedWalkersState();
}

class _TopRatedWalkersState extends State<TopRatedWalkers> {
  StreamSubscription _connectionChangeStream;

  var currentFilter = Filters.none;

  bool matched =false;
  bool isOffline = true;
  var petWalkers = [];
  double lat;
  double long;
  Geolocator _geolocator=Geolocator();
  Future<Position> _getCurrentLocation() async {
    return _geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best);}


  void connectionChanged(dynamic hasConnection) {
    setState(() {
      isOffline = !hasConnection;
    });
  }

  @override
  void initState() {
    super.initState();

    ConnectionStatusSingleton connectionStatus =
        ConnectionStatusSingleton.getInstance();
    ConnectionStatusSingleton.getInstance().checkConnection().then((value) => {
          setState(() {
            isOffline = !value;
          })
        });
    _connectionChangeStream =
        connectionStatus.connectionChange.listen(connectionChanged);

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Pet walkers'),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_outlined,
              size: 45.0,
              color: Colors.white,
            ),
            onPressed: () => Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => HomeScreenBody())),
          ),
          backgroundColor: Colors.green[900],
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () => Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => PetWalkersMap())),
          label: const Text('View on map'),
          icon: const Icon(Icons.map),
          backgroundColor: Colors.green,
        ),
        body: Column(
          children: <Widget>[
            isOffline ? NoConectionBar() : Container(),
            Container(
              margin: const EdgeInsets.all(10.0),
              decoration: new BoxDecoration(
                boxShadow: [new BoxShadow(
     color : Colors.white24,
     offset : Offset.zero,
     blurRadius : 0.0,
     spreadRadius : 3.0)]

              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        'Fliter options',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 12.0,
                            letterSpacing: 1.5,
                            fontWeight: FontWeight.w300),
                      )
                    ],
                  ),
                  Row(children: [
                    Container(
                      margin: const EdgeInsets.all(5.0),
                      child: RaisedButton(
                        onPressed: () {
                          setState(() {
                            currentFilter==Filters.rating?currentFilter=Filters.none:
                            currentFilter=Filters.rating;

                          });
                        },
                        color: (currentFilter==Filters.rating?Colors.green:Colors.blueGrey) ,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80.0),
                        ),
                        child: Ink(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(80.0),
                          ),
                          child: Container(
                            constraints: BoxConstraints(
                              maxWidth: 70.0,
                              maxHeight: 30.0,
                            ),
                            alignment: Alignment.center,
                            child: Text(
                              "Top rated",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12.0,
                                  letterSpacing: 2.0,
                                  fontWeight: FontWeight.w300),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(5.0),
                      child: RaisedButton(
                        onPressed: () {
                          setState(() {
                            currentFilter==Filters.distance?currentFilter=Filters.none:
                            currentFilter=Filters.distance;

                          });


                        },
                        color: (currentFilter==Filters.distance?Colors.green:Colors.blueGrey) ,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80.0),
                        ),
                        child: Ink(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(80.0),
                          ),
                          child: Container(
                            constraints: BoxConstraints(
                              maxWidth: 60.0,
                              maxHeight: 30.0,
                            ),
                            alignment: Alignment.center,
                            child: Text(
                              "Nearest",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12.0,
                                  letterSpacing: 2.0,
                                  fontWeight: FontWeight.w300),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Column(
                      children: <Widget>[
                        Text("Just for you"),
                        Padding(padding: EdgeInsets.only(left: 150)),
                        Switch(
                            activeColor: Colors.blue,
                            value: matched,
                            onChanged:(value){
                              setState((){
                                matched=value;
                                debugPrint("$matched");
                              });
                            }
                        ),
                      ],
                    ),
                  ]),
                ],
              ),
            ),

            getWalkers(context)
          ],
        ));
  }

  Widget getWalkers(BuildContext context){
    switch(currentFilter){
      case Filters.none:
        return buildDefault(context);
        break;
      case Filters.distance:
        return buildNear(context);
        break;
      case Filters.rating:
        return buildTopRated(context);
        break;

    }
  }

  Widget buildDefault(BuildContext context){
    final petOwner= Provider.of<PetOwner>(context);
    return isOffline
        ? FutureBuilder<File>(
        future: PetWalkersService().getTopPetWalkersOffline(petOwner.uid,matched),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if(snapshot.data==null){
              recordError(snapshot.error, snapshot.stackTrace, "Error loading walkers");
              return Text("");
            }
            else{
              return FutureBuilder<String>(
                  future: snapshot.data.readAsString(),
                  builder: (context, snapshot2) {
                    //debugPrint("${snapshot2.data}");
                    if(snapshot2.data==null){
                      petWalkers=[];
                    }
                    else{
                      petWalkers = jsonDecode(snapshot2.data);
                    }
                    //petWalkers = jsonDecode(snapshot2.data);
                    return Expanded(
                      child: ListView.builder(
                        itemCount: petWalkers == null
                            ? 0
                            : petWalkers.length,
                        itemBuilder:
                            (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () => Navigator.of(context)
                                .pushReplacement(MaterialPageRoute(
                                builder: (context) =>
                                    PetWalkerDetail(
                                        id: petWalkers[index]
                                        ['uid']))),
                            child: Card(
                              child: Row(
                                children: <Widget>[
                                  Image.asset('assets/dogWalker.png',
                                      height: 100, width: 100),
                                  Padding(
                                      padding:
                                      const EdgeInsets.all(30.0),
                                      child: Text(
                                        "${petWalkers[index]["displayName"]}",
                                        style: TextStyle(
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      )),
                                  Padding(
                                      padding:
                                      const EdgeInsets.all(2.0),
                                      child: Text(
                                        "${petWalkers[index]["avgRating"].toStringAsFixed(1)}",
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            fontWeight:
                                            FontWeight.w500,
                                            color: Colors.green[900]),
                                      )),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  });

            }

            //debugPrint("${snapshot.data.readAsString()}");
            //petWalkers = jsonDecode(snapshot.data.body);

          } else {
            return CircularProgressIndicator();
          }
        })
        : FutureBuilder(
        future: PetWalkersService().getPetWalkers(uid:petOwner.uid,matched:matched),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            petWalkers = jsonDecode(snapshot.data.body);
            return Expanded(
              child: ListView.builder(
                itemCount:
                petWalkers == null ? 0 : petWalkers.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () => Navigator.of(context)
                        .pushReplacement(MaterialPageRoute(
                        builder: (context) => PetWalkerDetail(
                            id: petWalkers[index]['uid']))),
                    child: Card(
                      child: Row(
                        children: <Widget>[
                          petWalkers[index]['photoURL'] != ''
                              ? Image.network(
                              petWalkers[index]['photoURL'],
                              height: 100,
                              width: 100)
                              : Image.asset('assets/dogWalker.png',
                              height: 100, width: 100),
                          Padding(
                              padding: const EdgeInsets.all(30.0),
                              child: Text(
                                "${petWalkers[index]["displayName"]}",
                                style: TextStyle(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.w500,
                                ),
                              )),
                          Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Text(
                                "${petWalkers[index]["avgRating"].toStringAsFixed(1)}",
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.green[900]),
                              )),
                        ],
                      ),
                    ),
                  );
                },
              ),
            );
          } else {
            return CircularProgressIndicator();
          }
        });
  }

  Widget buildTopRated(BuildContext context){
    final petOwner= Provider.of<PetOwner>(context);
    debugPrint("${petOwner.uid}");
    return isOffline
        ? FutureBuilder<File>(
        future: PetWalkersService().getTopPetWalkersOffline(petOwner.uid,matched),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if(snapshot.data==null){
              recordError(snapshot.error, snapshot.stackTrace, "Error loading walkers");
              return Text("");
            }
            else{
              return FutureBuilder<String>(
                  future: snapshot.data.readAsString(),
                  builder: (context, snapshot2) {
                    //debugPrint("${snapshot2.data}");
                    if(snapshot2.data==null){
                      petWalkers=[];
                    }
                    else{
                      petWalkers = jsonDecode(snapshot2.data);
                    }
                    return Expanded(
                      child: ListView.builder(
                        itemCount: petWalkers == null
                            ? 0
                            : petWalkers.length,
                        itemBuilder:
                            (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () => Navigator.of(context)
                                .pushReplacement(MaterialPageRoute(
                                builder: (context) =>
                                    PetWalkerDetail(
                                        id: petWalkers[index]
                                        ['uid']))),
                            child: Card(
                              child: Row(
                                children: <Widget>[
                                  Image.asset('assets/dogWalker.png',
                                      height: 100, width: 100),
                                  Padding(
                                      padding:
                                      const EdgeInsets.all(30.0),
                                      child: Text(
                                        "${petWalkers[index]["displayName"]}",
                                        style: TextStyle(
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      )),
                                  Padding(
                                      padding:
                                      const EdgeInsets.all(2.0),
                                      child: Text(
                                        "${petWalkers[index]["avgRating"].toStringAsFixed(1)}",
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            fontWeight:
                                            FontWeight.w500,
                                            color: Colors.green[900]),
                                      )),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  });

            }

            //debugPrint("${snapshot.data.readAsString()}");
            //petWalkers = jsonDecode(snapshot.data.body);

          } else {
            return CircularProgressIndicator();
          }
        })
        : FutureBuilder(
        future: PetWalkersService().getTopPetWalkers(petOwner.uid,matched),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            petWalkers = jsonDecode(snapshot.data.body);
            return Expanded(
              child: ListView.builder(
                itemCount:
                petWalkers == null ? 0 : petWalkers.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () => Navigator.of(context)
                        .pushReplacement(MaterialPageRoute(
                        builder: (context) => PetWalkerDetail(
                            id: petWalkers[index]['uid']))),
                    child: Card(
                      child: Row(
                        children: <Widget>[
                          petWalkers[index]['photoURL'] != ''
                              ? Image.network(
                              petWalkers[index]['photoURL'],
                              height: 100,
                              width: 100)
                              : Image.asset('assets/dogWalker.png',
                              height: 100, width: 100),
                          Padding(
                              padding: const EdgeInsets.all(30.0),
                              child: Text(
                                "${petWalkers[index]["displayName"]}",
                                style: TextStyle(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.w500,
                                ),
                              )),
                          Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Text(
                                "${petWalkers[index]["avgRating"].toStringAsFixed(1)}",
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.green[900]),
                              )),
                        ],
                      ),
                    ),
                  );
                },
              ),
            );
          } else {
            return CircularProgressIndicator();
          }
        });
  }

  Widget buildNear(BuildContext context){
    debugPrint("near");
    final petOwner= Provider.of<PetOwner>(context);
    debugPrint("${petOwner.uid}");

    if (isOffline){return Text("No disponible sin conexion");}
    else{
      return FutureBuilder(
          future: _getCurrentLocation(),
          builder: (context,snapshot){
            if(snapshot.connectionState==ConnectionState.done){
              lat= snapshot.data.latitude;
              long= snapshot.data.longitude;
              return FutureBuilder(
                  future: PetWalkersService().getNearPetWalkers(petOwner.uid,matched,lat,long),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      petWalkers = jsonDecode(snapshot.data.body);
                      return Expanded(
                        child: ListView.builder(
                          itemCount:
                          petWalkers == null ? 0 : petWalkers.length,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                              onTap: () => Navigator.of(context)
                                  .pushReplacement(MaterialPageRoute(
                                  builder: (context) => PetWalkerDetail(
                                      id: petWalkers[index]['uid']))),
                              child: Card(
                                child: Row(
                                  children: <Widget>[
                                    petWalkers[index]['photoURL'] != ''
                                        ? Image.network(
                                        petWalkers[index]['photoURL'],
                                        height: 100,
                                        width: 100)
                                        : Image.asset('assets/dogWalker.png',
                                        height: 100, width: 100),
                                    Padding(
                                        padding: const EdgeInsets.all(30.0),
                                        child: Text(
                                          "${petWalkers[index]["displayName"]}",
                                          style: TextStyle(
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        )),
                                    Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: Text(
                                          "${petWalkers[index]["avgRating"].toStringAsFixed(1)}",
                                          style: TextStyle(
                                              fontSize: 20.0,
                                              fontWeight: FontWeight.w500,
                                              color: Colors.green[900]),
                                        )),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      );
                    } else {
                      return CircularProgressIndicator();
                    }
                  });
            }
            else{return CircularProgressIndicator();}
    });}







  }
}
