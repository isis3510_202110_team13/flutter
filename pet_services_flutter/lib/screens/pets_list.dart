import 'dart:io';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:pet_services_flutter/models/pet_owner.dart';
import 'package:pet_services_flutter/screens/create_pet.dart';
import 'package:pet_services_flutter/screens/pet_detail.dart';
import 'package:pet_services_flutter/services/pet_owners_service.dart';
import 'package:pet_services_flutter/screens/homeScreenBody.dart';
import 'package:pet_services_flutter/services/connection_status_singleton.dart';
import 'dart:async';
import 'dart:convert';
import 'package:pet_services_flutter/services/auth.dart';
import 'package:provider/provider.dart';
import 'no_conection_bar.dart';

class PetsList extends StatefulWidget {
  @override
  _PetsListState createState() => _PetsListState();
}

class _PetsListState extends State<PetsList> {
  StreamSubscription _connectionChangeStream;

  bool isOffline = true;
  var petWalkers = [];
  var uid='';
  var pets =[];

  void connectionChanged(dynamic hasConnection) {
    setState(() {
      isOffline = !hasConnection;
    });
  }

  @override
  void initState() {
    super.initState();
    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
    ConnectionStatusSingleton connectionStatus =
        ConnectionStatusSingleton.getInstance();
    ConnectionStatusSingleton.getInstance().checkConnection().then((value) => {
      setState(() {
        isOffline = !value;
      })
    });
    _connectionChangeStream =
        connectionStatus.connectionChange.listen(connectionChanged);
  }

  @override
  Widget build(BuildContext context) {
    final petOwner= Provider.of<PetOwner>(context);

    //final petOwner = AuthServiceF().petOwner.first;
    return Scaffold(
        appBar: AppBar(
          title: Text('My pets'),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_outlined,
              size: 45.0,
              color: Colors.white,
            ),
            onPressed: () => Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => HomeScreenBody())),
          ),
          backgroundColor: Colors.green[900],
        ),
        body: Column(
          children: <Widget>[
            isOffline
                ? NoConectionBar()
                : Container(),
            isOffline?
            FutureBuilder<File>(
                future: PetOwnersService().getPetsOffline(petOwner.uid),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if(snapshot.hasError){
                      FirebaseCrashlytics.instance.log("Error loading pets");
                    }
                    if(snapshot.data==null){
                      debugPrint("aaaaaa");
                      recordError(snapshot.error, snapshot.stackTrace, "Error loading pets");
                      return Text("");
                    }
                    else{
                      return
                        FutureBuilder<String>(
                            future: snapshot.data.readAsString(),
                            builder: (context,snapshot2){
                              if(snapshot.hasError){
                                recordError(snapshot.error, snapshot.stackTrace, "Error loading pets");

                                FirebaseCrashlytics.instance.log("Error loading pets");
                                return Text("${snapshot.error}");
                              }
                              else{
                                if(snapshot2.data==null){
                                  pets=[];
                                }
                                else{
                                  pets = jsonDecode(snapshot2.data);
                                }
                                return Expanded(
                                  child: ListView.builder(
                                    itemCount: pets == null ? 0 : pets.length,
                                    itemBuilder: (BuildContext context, int index) {
                                      return GestureDetector(
                                        onTap: () => Navigator.of(context).pushReplacement(
                                            MaterialPageRoute(
                                                builder: (context) => PetDetail(
                                                  id: "${pets[index]['id']}",
                                                  name:pets[index]['name'],
                                                  breed:pets[index]['breed'],
                                                  age:"${pets[index]['age']}",
                                                  size:pets[index]['size'],

                                                  photoURL: pets[index]['photoURL'],






                                                )

                                            )),
                                        child: Card(
                                          child: Row(
                                            children: <Widget>[
                                              Image.asset('assets/default-paw.jpeg',
                                                  height: 100, width: 100),
                                              Padding(
                                                  padding: const EdgeInsets.all(30.0),
                                                  child: Text(
                                                    "${pets[index]["name"]}",
                                                    style: TextStyle(
                                                      fontSize: 20.0,
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                  )),
                                              Padding(
                                                  padding: const EdgeInsets.all(2.0),
                                                  child: Text(
                                                    "Age:${pets[index]["age"]}",
                                                    style: TextStyle(
                                                        fontSize: 20.0,
                                                        fontWeight: FontWeight.w500,
                                                        color: Colors.green[900]),
                                                  )),
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                );

                              }


                            });
                    }
                    //pets = jsonDecode(snapshot.data.body);




                  } else {
                    return CircularProgressIndicator();
                  }
                })

                :

            FutureBuilder(
                future: PetOwnersService().getPets(petOwner.uid),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {

                    if(snapshot.data==null){
                      pets=[];
                    }
                    else{
                      pets = jsonDecode(snapshot.data.body);
                    }
                    return
                      Expanded(
                        child: ListView.builder(
                          itemCount: pets == null ? 0 : pets.length,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                              onTap: () => Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                      builder: (context) => PetDetail(
                                        id: "${pets[index]['id']}",
                                        name:pets[index]['name'],
                                        breed:pets[index]['breed'],
                                        age:"${pets[index]['age']}",
                                        size:pets[index]['size'],

                                        photoURL: pets[index]['photoURL'],






                                      )

                                  )),
                              child: Card(
                                child: Row(
                                  children: <Widget>[
                                    pets[index]['photoURL'] != ''
                                        ? Image.network(
                                        pets[index]['photoURL'],
                                        height: 100,
                                        width: 100)
                                        : Image.asset('assets/dogWalker.png',
                                        height: 100, width: 100),
                                    Padding(
                                        padding: const EdgeInsets.all(30.0),
                                        child: Text(
                                          "${pets[index]["name"]}",
                                          style: TextStyle(
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        )),
                                    Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: Text(
                                          "Age:${pets[index]["age"]}",
                                          style: TextStyle(
                                              fontSize: 20.0,
                                              fontWeight: FontWeight.w500,
                                              color: Colors.green[900]),
                                        )),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      );
                  } else {
                    return CircularProgressIndicator();
                  }
                }),
          ],
        ),

        floatingActionButton: FloatingActionButton.extended(
        onPressed: () => Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => CreatePet(uid:petOwner.uid))),
    label: const Text('Add pet'),
    icon: const Icon(Icons.add),
    backgroundColor: Colors.green,
    ),
    );
  }
}
void  recordError(error,stackTrace,reason) async{
  //FirebaseCrashlytics.instance.crash();
  await FirebaseCrashlytics.instance.sendUnsentReports();
  await FirebaseCrashlytics.instance.recordError(
      error,
      stackTrace,
      reason:reason
  );

}
