import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pet_services_flutter/widgets/alert.dart';
import 'package:pet_services_flutter/services/storage_service.dart';
import 'package:path/path.dart';
import 'no_conection_bar.dart';
import 'package:pet_services_flutter/services/connection_status_singleton.dart';
import 'dart:async';
import '../services/pet_owners_service.dart';
import 'package:pet_services_flutter/screens/pet_owner_detail.dart';
import 'package:pet_services_flutter/models/pet_owner.dart';
import 'package:provider/provider.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';

class ProfileScreen extends StatefulWidget {
  final String uid;

  ProfileScreen({this.uid});

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool status = false;
  bool _status = true;
  bool isOffline = true;
  String name = '';
  String email = '';
  String mobile = '';
  String password = '';
  File _image;
  var user =[];

  final FocusNode myFocusNode = FocusNode();
  StreamSubscription _connectionChangeStream;

  @override
  void initState() {
    super.initState();

    ConnectionStatusSingleton connectionStatus =
        ConnectionStatusSingleton.getInstance();
    ConnectionStatusSingleton.getInstance().checkConnection().then((value) => {
          setState(() {
            isOffline = !value;
          })
        });

    _connectionChangeStream =
        connectionStatus.connectionChange.listen(connectionChanged);
  }

  final picker = ImagePicker();

  Future getImageCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        debugPrint('No image selected.');
      }
    });
  }

  Future getImageGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        debugPrint('No image selected.');
      }
    });
  }

  void connectionChanged(dynamic hasConnection) {
    setState(() {
      isOffline = !hasConnection;
    });
  }

  saveInformation(context) async {
    AuthyAlert.showInfoDialog(context, "Information saved",
        "The profile information of $name has been saved properly");
    var user = new Map<String, dynamic>();
    user["name"] = name;
    user["breed"] = email;
    user["age"] = mobile;
    user["size"] = password;

    var newId;
    var photoURL =
        "https://firebasestorage.googleapis.com/v0/b/pet-services-d7b69.appspot.com/o/default_images%2Fprofileplaceholder.jpg?alt=media&token=eb6e3775-ece9-4df4-aecb-563f09239e8f";
    if (_image != null) {
      var fileName =
          "${widget.uid}-${DateTime.now().millisecondsSinceEpoch}-${basename(_image.path)}";

      await StorageService().uploadUserFile(fileName, _image);
      photoURL = await StorageService().getURLUser(fileName);
    }

    PetOwnersService().updateUser(widget.uid, user).then((value) => {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => PetOwnerDetail(
                  id: "${jsonDecode(value.body)["id"]}",
                  name: name,
                  email: email,
                  mobile: mobile,
                  password: password,
                  photoURL: photoURL)))
        });
  }


  Widget _getActionButtons(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 45.0),
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: Container(
                  child: new ElevatedButton(
                child: new Text("Save"),
                style: ElevatedButton.styleFrom(
                  textStyle: TextStyle(
                    color: isOffline ? Colors.blueGrey : Colors.green,
                  ),
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(20.0)),
                ),
                onPressed: () {
                  setState(() {
                    _status = true;
                    isOffline
                        ? AuthyAlert.showErrorDialog(
                            context, 'No internet conection')
                        : saveInformation(context);
                    FocusScope.of(context).requestFocus(new FocusNode());
                  });
                },
              )),
            ),
            flex: 2,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Container(
                  child: new RaisedButton(
                child: new Text("Cancel"),
                textColor: Colors.white,
                color: Colors.red,
                onPressed: () {
                  setState(() {
                    _status = true;
                    FocusScope.of(context).requestFocus(new FocusNode());
                  });
                },
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(20.0)),
              )),
            ),
            flex: 2,
          ),
        ],
      ),
    );
  }

  Widget _getEditIcon() {
    return new GestureDetector(
      child: new CircleAvatar(
        backgroundColor: Colors.red,
        radius: 14.0,
        child: new Icon(
          Icons.edit,
          size: 16.0,
        ),
      ),
      onTap: () {
        setState(() {
          _status = false;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final petOwner = Provider.of<PetOwner>(context);
    return new Scaffold(
        appBar: AppBar(
          title: Text('Profile'),
          backgroundColor: Colors.green[900],
        ),
        body: new Container(
          child: new ListView(
            children: <Widget>[
              SizedBox(
                height: 800.0,
                width: 200.0,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    isOffline ? NoConectionBar() : Container(),
                    new Container(
                      child: new Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 20.0),
                            child:
                                new Stack(fit: StackFit.loose, children: <Widget>[
                              new Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Container(
                                    alignment: Alignment(0.0, 1.0),
                                    child: _image == null
                                        ? CircleAvatar(
                                            backgroundImage:
                                                AssetImage('assets/profile.png'),
                                            radius: 60.0,
                                            backgroundColor: Colors.white,
                                          )
                                        : CircleAvatar(
                                            backgroundImage: FileImage(_image),
                                            radius: 60.0,
                                            backgroundColor: Colors.transparent,
                                          ),
                                  ),
                                  new Padding(
                                    padding: EdgeInsets.only(top: 110.0),
                                    child: new Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        GestureDetector(
                                            onTap: () {
                                              showDialog(
                                                  context: context,
                                                  barrierDismissible: false,
                                                  builder: (context) {
                                                    return AlertDialog(
                                                      title: Text('Origin',
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.blue)),
                                                      content:
                                                          SingleChildScrollView(
                                                        child: ListBody(
                                                          children: [
                                                            Text("Select origin",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .black)),
                                                            RaisedButton(
                                                              onPressed: () {
                                                                getImageCamera();
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              },
                                                              color: isOffline
                                                                  ? Colors
                                                                      .blueGrey
                                                                  : Colors.green,
                                                              shape:
                                                                  RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            80.0),
                                                              ),
                                                              child: Ink(
                                                                decoration:
                                                                    BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              80.0),
                                                                ),
                                                                child: Container(
                                                                  constraints:
                                                                      BoxConstraints(
                                                                    maxWidth:
                                                                        100.0,
                                                                    maxHeight:
                                                                        40.0,
                                                                  ),
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                  child: Text(
                                                                    "From camera",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontSize:
                                                                            12.0,
                                                                        letterSpacing:
                                                                            2.0,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w300),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              height: 10,
                                                            ),
                                                            RaisedButton(
                                                              onPressed: () {
                                                                getImageGallery();
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              },
                                                              color: isOffline
                                                                  ? Colors
                                                                      .blueGrey
                                                                  : Colors.green,
                                                              shape:
                                                                  RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            80.0),
                                                              ),
                                                              child: Ink(
                                                                decoration:
                                                                    BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              80.0),
                                                                ),
                                                                child: Container(
                                                                  constraints:
                                                                      BoxConstraints(
                                                                    maxWidth:
                                                                        100.0,
                                                                    maxHeight:
                                                                        40.0,
                                                                  ),
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                  child: Text(
                                                                    "From gallery",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontSize:
                                                                            12.0,
                                                                        letterSpacing:
                                                                            2.0,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w300),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      actions: [],
                                                    );
                                                  });
                                              //getImage();
                                            },
                                            child: CircleAvatar(
                                              backgroundColor: Colors.red,
                                              radius: 25.0,
                                              child: new Icon(
                                                Icons.camera_alt,
                                              ),
                                            )),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ]),
                          )
                        ],
                      ),
                    ),
                    isOffline
                        ? FutureBuilder<File>(
                            future:
                                PetOwnersService().getPetsOffline(petOwner.uid),
                            builder: (context, snapshot) {
                              if (snapshot.connectionState ==
                                  ConnectionState.done) {
                                if (snapshot.hasError) {
                                  FirebaseCrashlytics.instance
                                      .log("Error loading pets");
                                }
                                if (snapshot.data == null) {
                                  debugPrint("snapshot is null");
                                  recordError(snapshot.error, snapshot.stackTrace,
                                      "Error loading pets");
                                  return Text("");
                                } else {
                                  return FutureBuilder<String>(
                                      future: snapshot.data.readAsString(),
                                      builder: (context, snapshot2) {
                                        if (snapshot.hasError) {
                                          recordError(
                                              snapshot.error,
                                              snapshot.stackTrace,
                                              "Error loading pets");

                                          FirebaseCrashlytics.instance
                                              .log("Error loading pets");
                                          return Text("${snapshot.error}");
                                        } else {
                                          if (snapshot2.data == null) {
                                            user = [];
                                          } else {
                                            user = jsonDecode(snapshot2.data);
                                          }
                                          return new Container(
                                            child: Padding(
                                              padding:
                                                  EdgeInsets.only(bottom: 25.0),
                                              child: new Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: <Widget>[
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 25.0,
                                                          right: 25.0,
                                                          top: 25.0),
                                                      child: new Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        children: <Widget>[
                                                          new Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            mainAxisSize:
                                                                MainAxisSize.min,
                                                            children: <Widget>[
                                                              new Text(
                                                                'Personal Information',
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        18.0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              ),
                                                            ],
                                                          ),
                                                          new Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .end,
                                                            mainAxisSize:
                                                                MainAxisSize.min,
                                                            children: <Widget>[
                                                              _status
                                                                  ? _getEditIcon()
                                                                  : new Container(),
                                                            ],
                                                          )
                                                        ],
                                                      )),
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 25.0,
                                                          right: 25.0,
                                                          top: 25.0),
                                                      child: new Row(
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        children: <Widget>[
                                                          new Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            mainAxisSize:
                                                                MainAxisSize.min,
                                                            children: <Widget>[
                                                              new Text(
                                                                'Name',
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        16.0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      )),
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 25.0,
                                                          right: 25.0,
                                                          top: 2.0),
                                                      child: new Row(
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        children: <Widget>[
                                                          new Flexible(
                                                            child: new TextField(
                                                              decoration:
                                                                  const InputDecoration(
                                                                hintText:
                                                                    "Enter Your Name",
                                                              ),
                                                              // TODO : get from firebase the original input
                                                              enabled: !_status,
                                                              autofocus: !_status,
                                                            ),
                                                          ),
                                                        ],
                                                      )),
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 25.0,
                                                          right: 25.0,
                                                          top: 25.0),
                                                      child: new Row(
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        children: <Widget>[
                                                          new Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            mainAxisSize:
                                                                MainAxisSize.min,
                                                            children: <Widget>[
                                                              new Text(
                                                                'Email ID',
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        16.0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      )),
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 25.0,
                                                          right: 25.0,
                                                          top: 2.0),
                                                      child: new Row(
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        children: <Widget>[
                                                          new Flexible(
                                                            child: new TextField(
                                                              decoration:
                                                                  const InputDecoration(
                                                                      hintText:
                                                                          "Enter Email ID"),
                                                              // TODO : get from firebase the original input
                                                              enabled: !_status,
                                                            ),
                                                          ),
                                                        ],
                                                      )),
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 25.0,
                                                          right: 25.0,
                                                          top: 25.0),
                                                      child: new Row(
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        children: <Widget>[
                                                          new Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            mainAxisSize:
                                                                MainAxisSize.min,
                                                            children: <Widget>[
                                                              new Text(
                                                                'Mobile',
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        16.0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      )),
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 25.0,
                                                          right: 25.0,
                                                          top: 2.0),
                                                      child: new Row(
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        children: <Widget>[
                                                          new Flexible(
                                                            child: new TextField(
                                                              decoration:
                                                                  const InputDecoration(
                                                                      hintText:
                                                                          "Enter Mobile Number"),
                                                              // TODO : get from firebase the original input
                                                              enabled: !_status,
                                                            ),
                                                          ),
                                                        ],
                                                      )),
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 25.0,
                                                          right: 25.0,
                                                          top: 25.0),
                                                      child: new Row(
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Expanded(
                                                            child: Container(
                                                              child: new Text(
                                                                'Password',
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        16.0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              ),
                                                            ),
                                                            flex: 2,
                                                          ),
                                                          Expanded(
                                                            child: Container(
                                                              child: new Text(
                                                                'Confirm Password',
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        16.0,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              ),
                                                            ),
                                                            flex: 2,
                                                          ),
                                                        ],
                                                      )),
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 25.0,
                                                          right: 25.0,
                                                          top: 2.0),
                                                      child: new Row(
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Flexible(
                                                            child: Padding(
                                                              padding:
                                                                  EdgeInsets.only(
                                                                      right:
                                                                          10.0),
                                                              child:
                                                                  new TextField(
                                                                decoration:
                                                                    const InputDecoration(
                                                                        hintText:
                                                                            "Enter Password"),
                                                                enabled: !_status,
                                                              ),
                                                            ),
                                                            flex: 2,
                                                          ),
                                                          Flexible(
                                                            child: new TextField(
                                                              decoration:
                                                                  const InputDecoration(
                                                                      hintText:
                                                                          "Confirm Password"),
                                                              enabled: !_status,
                                                            ),
                                                            flex: 2,
                                                          ),
                                                        ],
                                                      )),
                                                  !_status
                                                      ? _getActionButtons(context)
                                                      : new Container(),
                                                ],
                                              ),
                                            ),
                                          );
                                        }
                                      });
                                }
                              } else {
                                return CircularProgressIndicator();
                              }
                            })
                        : FutureBuilder(
                            future: PetOwnersService().getPets(petOwner.uid),
                            builder: (context, snapshot) {
                              if (snapshot.connectionState ==
                                  ConnectionState.done) {
                                if (snapshot.data == null) {
                                  user = [];
                                } else {
                                  user = jsonDecode(snapshot.data.body);
                                  //new Map<String, dynamic>.from(snapshot.value);
                                }
                                return new Expanded(
                                  child: GestureDetector(
                                        onTap: () =>
                                            Navigator.of(context).pushReplacement(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  PetOwnerDetail(
                                                    id: "${user[0]['id']}",
                                                    name: user[0]['name'],
                                                    email: user[0]['email'],
                                                    mobile:
                                                        "${user[0]['mobile']}",
                                                    password: user[0]
                                                        ['password'],
                                                    photoURL: user[0]
                                                        ['photoURL'],
                                                  )),
                                        ),
                                        child: Padding(
                                          padding: EdgeInsets.only(bottom: 25.0),
                                          child: new Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 25.0,
                                                      right: 25.0,
                                                      top: 25.0),
                                                  child: new Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    children: <Widget>[
                                                      new Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        mainAxisSize:
                                                            MainAxisSize.min,
                                                        children: <Widget>[
                                                          new Text(
                                                            'Personal Information',
                                                            style: TextStyle(
                                                                fontSize: 18.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                        ],
                                                      ),
                                                      new Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment.end,
                                                        mainAxisSize:
                                                            MainAxisSize.min,
                                                        children: <Widget>[
                                                          _status
                                                              ? _getEditIcon()
                                                              : new Container(),
                                                        ],
                                                      )
                                                    ],
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 25.0,
                                                      right: 25.0,
                                                      top: 25.0),
                                                  child: new Row(
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    children: <Widget>[
                                                      new Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        mainAxisSize:
                                                            MainAxisSize.min,
                                                        children: <Widget>[
                                                          new Text(
                                                            'Name',
                                                            style: TextStyle(
                                                                fontSize: 16.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 25.0,
                                                      right: 25.0,
                                                      top: 2.0),
                                                  child: new Row(
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    children: <Widget>[
                                                      new Flexible(
                                                        child: new TextField(
                                                          decoration:
                                                              new InputDecoration(
                                                            hintText:
                                                                "${user[0]["name"]}",
                                                          ),
                                                          // TODO : get from firebase the original input
                                                          enabled: !_status,
                                                          autofocus: !_status,
                                                        ),
                                                      ),
                                                    ],
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 25.0,
                                                      right: 25.0,
                                                      top: 25.0),
                                                  child: new Row(
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    children: <Widget>[
                                                      new Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        mainAxisSize:
                                                            MainAxisSize.min,
                                                        children: <Widget>[
                                                          new Text(
                                                            'Email ID',
                                                            style: TextStyle(
                                                                fontSize: 16.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 25.0,
                                                      right: 25.0,
                                                      top: 2.0),
                                                  child: new Row(
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    children: <Widget>[
                                                      new Flexible(
                                                        child: new TextField(
                                                          decoration:
                                                              new InputDecoration(
                                                                  hintText:
                                                                      "${user[0]["email"]}"),
                                                          enabled: !_status,
                                                        ),
                                                      ),
                                                    ],
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 25.0,
                                                      right: 25.0,
                                                      top: 25.0),
                                                  child: new Row(
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    children: <Widget>[
                                                      new Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        mainAxisSize:
                                                            MainAxisSize.min,
                                                        children: <Widget>[
                                                          new Text(
                                                            'Mobile',
                                                            style: TextStyle(
                                                                fontSize: 16.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 25.0,
                                                      right: 25.0,
                                                      top: 2.0),
                                                  child: new Row(
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    children: <Widget>[
                                                      new Flexible(
                                                        child: new TextField(
                                                          decoration:
                                                              new InputDecoration(
                                                                  hintText:
                                                                  "${user[0]["mobile"]}"),
                                                          // TODO : get from firebase the original input
                                                          enabled: !_status,
                                                        ),
                                                      ),
                                                    ],
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 25.0,
                                                      right: 25.0,
                                                      top: 25.0),
                                                  child: new Row(
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: <Widget>[
                                                      Expanded(
                                                        child: Container(
                                                          child: new Text(
                                                            'Password',
                                                            style: TextStyle(
                                                                fontSize: 16.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                        ),
                                                        flex: 2,
                                                      ),
                                                      Expanded(
                                                        child: Container(
                                                          child: new Text(
                                                            'Confirm Password',
                                                            style: TextStyle(
                                                                fontSize: 16.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                        ),
                                                        flex: 2,
                                                      ),
                                                    ],
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 25.0,
                                                      right: 25.0,
                                                      top: 2.0),
                                                  child: new Row(
                                                    mainAxisSize:
                                                        MainAxisSize.max,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: <Widget>[
                                                      Flexible(
                                                        child: Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  right: 10.0),
                                                          child: new TextField(
                                                            decoration:
                                                                const InputDecoration(
                                                                    hintText:
                                                                        "Enter Password"),
                                                            enabled: !_status,
                                                          ),
                                                        ),
                                                        flex: 2,
                                                      ),
                                                      Flexible(
                                                        child: new TextField(
                                                          decoration:
                                                              const InputDecoration(
                                                                  hintText:
                                                                      "Confirm Password"),
                                                          enabled: !_status,
                                                        ),
                                                        flex: 2,
                                                      ),
                                                    ],
                                                  )),
                                              !_status
                                                  ? _getActionButtons(context)
                                                  : new Container(),
                                            ],
                                          ),
                                        ),
                                      )
                                  );
                              } else {
                                return CircularProgressIndicator();
                              }
                            }),
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  void recordError(error, stackTrace, reason) async {
    await FirebaseCrashlytics.instance.sendUnsentReports();
    await FirebaseCrashlytics.instance
        .recordError(error, stackTrace, reason: reason);
  }

}
