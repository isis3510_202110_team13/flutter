
import 'package:flutter/material.dart';
import 'package:pet_services_flutter/services/pet_walkers_service.dart';
import 'package:pet_services_flutter/screens/home.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pet_services_flutter/services/connection_status_singleton.dart';
import 'dart:async';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class PetWalkerDetail extends StatefulWidget {
  final String id;

  PetWalkerDetail({this.id});

  @override
  _PetWalkerDetailState createState() => _PetWalkerDetailState();
}

class _PetWalkerDetailState extends State<PetWalkerDetail> {
  StreamSubscription _connectionChangeStream;
  bool isOffline = false;
  var petWalkers = [];
  var petWalker;

  void connectionChanged(dynamic hasConnection) {
    setState(() {
      isOffline = !hasConnection;
    });
  }

  @override
  void initState() {
    super.initState();

    ConnectionStatusSingleton connectionStatus =
    ConnectionStatusSingleton.getInstance();
    _connectionChangeStream =
        connectionStatus.connectionChange.listen(connectionChanged);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Detail ' + widget.id),
          leading: IconButton(
            icon: Icon(
              FontAwesomeIcons.solidArrowAltCircleLeft,
              size: 45.0,
              color: Colors.green,
            ),
            onPressed: () => Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => HomeScreen())),
          ),
          backgroundColor: Colors.green[900],
          actions: <Widget>[
            Builder(builder: (BuildContext context) {
//5
              return FlatButton(
                child: const Text('Sign out'),
                textColor: Theme.of(context).buttonColor,
                onPressed: () => Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => HomeScreen())),
              );
            })
          ],
        ),
        body: Column(
          children: <Widget>[
            isOffline ? Text('No conection') : Text('Conected'),
            FutureBuilder(
                future: PetWalkersService().getPetWalker(widget.id),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    } else {
                      return Column(// here only return is missing
                          children: [
                            Container(
                                width: double.infinity,
                                height: 150,
                                child: Container(
                                    alignment: Alignment(0.0, 2.5),
                                    child: snapshot.data['photoUrl'] == ''
                                        ? CircleAvatar(
                                      backgroundImage:
                                      AssetImage('assets/dogWalker.png'),
                                      radius: 60.0,
                                    )
                                        : CircleAvatar(
                                      backgroundImage: NetworkImage(
                                          snapshot.data['photoUrl']),
                                      radius: 60.0,
                                    ))),
                            SizedBox(
                              height: 80,
                            ),
                            Text(snapshot.data['displayName'],
                                style: TextStyle(
                                    fontSize: 25.0,
                                    color: Colors.blueGrey,
                                    letterSpacing: 2.0,
                                    fontWeight: FontWeight.w400)),
                            RatingBar.builder(
                              initialRating: snapshot.data['rating'],
                              minRating: 1,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemCount: 5,
                              itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              onRatingUpdate: (rating) {
                                print(rating);
                              },
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(snapshot.data["description"],
                                style: TextStyle(
                                    fontSize: 15.0,
                                    color: Colors.black45,
                                    letterSpacing: 2.0,
                                    fontWeight: FontWeight.w300)),
                            RaisedButton(
                              onPressed: () {},
                              color: Colors.green,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(80.0),
                              ),
                              child: Ink(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(80.0),
                                ),
                                child: Container(
                                  constraints: BoxConstraints(
                                    maxWidth: 100.0,
                                    maxHeight: 40.0,
                                  ),
                                  alignment: Alignment.center,
                                  child: Text(
                                    "Hire now",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12.0,
                                        letterSpacing: 2.0,
                                        fontWeight: FontWeight.w300),
                                  ),
                                ),
                              ),
                            ),
                            Card(
                                margin: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 30.0),
                                child:
                                Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children:[
                                      Padding(

                                          padding: const EdgeInsets.all(8.0),
                                          child: Expanded(
                                              child: Column(children: [
                                                Text('Last walks',
                                                    style: TextStyle(
                                                        color: Colors.green,
                                                        fontSize: 22.0,
                                                        fontWeight: FontWeight.w600)),
                                                SizedBox(
                                                  height: 30,
                                                ),
                                              ])
                                          )
                                      )

                                    ])




                            ),
                          ]);
                    }
                  } else if (snapshot.hasError) {
                    Text('no data');
                  }
                  return CircularProgressIndicator();
                })
          ],
        ));
  }
}
