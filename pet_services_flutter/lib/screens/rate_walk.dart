import 'package:flutter/material.dart';
import 'package:pet_services_flutter/home/homeAuth.dart';
import 'package:pet_services_flutter/screens/homeScreenBody.dart';
import 'package:pet_services_flutter/screens/ratings.dart';
import 'package:pet_services_flutter/services/walk_service.dart';

class RateWalk extends StatefulWidget {
  @override
  _RateWalkState createState() => _RateWalkState();
}

class _RateWalkState extends State<RateWalk> {
  int _rating;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Rate the walk")),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Rating((rating) {
                setState(() {
                  _rating = rating;
                });
              }, 5),
              SizedBox(
                  height: 44,
                  child: (_rating != null && _rating != 0)
                      ? Text("You selected $_rating rating",
                          style: TextStyle(fontSize: 18))
                      : SizedBox.shrink()),
              ElevatedButton(
                  onPressed: () async {
                    var walk = new Map<String, dynamic>();
                    walk["status"] = "finished";
                    walk["rating"] = _rating;

                    WalksService().postWalk(walk).then((value) {
                      print(value);
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (context) => HomeScreenBody()));
                    });
                  },
                  child: Text('Start Walk!'),
                  style: ElevatedButton.styleFrom(
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0),
                      ),
                      primary: Colors.green)),
            ],
          ),
        ));
  }
}