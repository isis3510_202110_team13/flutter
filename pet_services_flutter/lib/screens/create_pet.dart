import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pet_services_flutter/screens/pet_detail.dart';
import 'package:pet_services_flutter/screens/pets_list.dart';
import 'package:pet_services_flutter/services/connection_status_singleton.dart';
import 'package:pet_services_flutter/services/storage_service.dart';
import 'package:pet_services_flutter/widgets/alert.dart';
import '../services/pet_owners_service.dart';
import 'dart:io';
import 'dart:async';
import 'no_conection_bar.dart';

class CreatePet extends StatefulWidget {
  final String uid;
  CreatePet({this.uid});
  @override
  _CreatePetState createState() => _CreatePetState();
}

class _CreatePetState extends State<CreatePet> {
  StreamSubscription _connectionChangeStream;
  final _formKey = GlobalKey<FormState>();

  bool isOffline = true;
  String name = '';
  String breed = '';
  String age = '';
  String size = 'small';
  List<String> petTags = [];
  var defaultTags = [
    {
      "name": "Special care",
      "is_active": false,
    },
    {
      "name": "Barker",
      "is_active": false,
    },
    {
      "name": "Shy",
      "is_active": false,
    },
    {
      "name": "Curious",
      "is_active": false,
    },
    {
      "name": "Alert",
      "is_active": false,
    },
    {
      "name": "Friendly",
      "is_active": false,
    },
    {
      "name": "Disobidient",
      "is_active": false,
    },
    {
      "name": "Cat-Friendly",
      "is_active": false,
    },
    {
      "name": "Not-Cat-Friendly",
      "is_active": false,
    },
  ];

  File _image;
  List<Map<String,dynamic>> tagsEncode(List<String> t) {
    List<Map<String,dynamic>> list=[];
    for (int i=0; i<t.length;i++){
      var tag = new Map<String, dynamic>();
      tag["name"]=t[i];
      list.add(tag);
    }
    print(list);
    return list;
  }

  final picker = ImagePicker();

  Future getImageCamera() async {
    // FilePickerResult result = await FilePicker.platform.pickFiles();
    //
    // if(result != null) {
    //   _image = File(result.files.single.path);
    // } else {
    //   // User canceled the picker
    // }

    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Future getImageGallery() async {
    // FilePickerResult result = await FilePicker.platform.pickFiles();
    //
    // if(result != null) {
    //   _image = File(result.files.single.path);
    // } else {
    //   // User canceled the picker
    // }

    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  createpet(context) async {
    AuthyAlert.showInfoDialog(
        context, "Registrando a $name", "El proceso tomará unos segundos");
    var pet = new Map<String,dynamic>();
    pet["name"] = name;
    pet["breed"] = breed;
    pet["age"] = age;
    pet["size"] = size;
    pet["Tags"]=tagsEncode(petTags);

    var newId;
    var photoURL =
        "https://firebasestorage.googleapis.com/v0/b/pet-services-d7b69.appspot.com/o/default_images%2Fdefault-paw.jpeg?alt=media&token=c4d92952-09fe-4ca5-9416-64334e14544e";
    if (_image != null) {
      var fileName =
          "${widget.uid}-${DateTime.now().millisecondsSinceEpoch}-${basename(_image.path)}";

      await StorageService().uploadPetFile(fileName, _image);
      photoURL = await StorageService().getURL(fileName);
    }

    pet["photoURL"] = photoURL;
    //debugPrint("$pet");
    PetOwnersService().postPet(widget.uid, pet).then((value) => {

          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => PetDetail(
                  id: "${jsonDecode(value.body)["id"]}",
                  name: name,
                  breed: breed,
                  age: age,
                  size: size,
                  photoURL: photoURL)))
        });
  }

  void connectionChanged(dynamic hasConnection) {
    setState(() {
      isOffline = !hasConnection;
    });
  }

  @override
  void initState() {
    super.initState();

    ConnectionStatusSingleton connectionStatus =
        ConnectionStatusSingleton.getInstance();
    ConnectionStatusSingleton.getInstance().checkConnection().then((value) => {
          setState(() {
            isOffline = !value;
          })
        });
    _connectionChangeStream =
        connectionStatus.connectionChange.listen(connectionChanged);
  }

  String dropdownValue = 'One';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Create Pet'),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_outlined,
              size: 45.0,
              color: Colors.white,
            ),
            onPressed: () => Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => PetsList())),
          ),
          backgroundColor: Colors.green[900],
        ),
        body: SingleChildScrollView(
            child: Column(
          children: <Widget>[
            isOffline ? NoConectionBar() : Container(),
            Container(
                padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
                child: Form(
                    key: _formKey,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: double.infinity,
                            height: 150,
                            child: Stack(
                              fit: StackFit.loose,
                              children: <Widget>[
                                Container(
                                    alignment: Alignment(0.0, 1.0),
                                    child: _image == null
                                        ? CircleAvatar(
                                            backgroundImage: AssetImage(
                                                'assets/default-paw.jpeg'),
                                            radius: 60.0,
                                          )
                                        : CircleAvatar(
                                            backgroundImage: FileImage(_image),
                                            radius: 60.0,
                                          )),
                                Padding(
                                    padding: EdgeInsets.only(
                                        top: 110.0, left: 100.0),
                                    child: new Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        GestureDetector(
                                            onTap: () {
                                              showDialog(
                                                  context: context,
                                                  barrierDismissible: false,
                                                  builder: (context) {
                                                    return AlertDialog(
                                                      title: Text('Origen',
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.blue)),
                                                      content:
                                                          SingleChildScrollView(
                                                        child: ListBody(
                                                          children: [
                                                            Text(
                                                                "Select origin",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .black)),
                                                            RaisedButton(
                                                              onPressed: () {
                                                                getImageCamera();
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              },
                                                              color: isOffline
                                                                  ? Colors
                                                                      .blueGrey
                                                                  : Colors
                                                                      .green,
                                                              shape:
                                                                  RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            80.0),
                                                              ),
                                                              child: Ink(
                                                                decoration:
                                                                    BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              80.0),
                                                                ),
                                                                child:
                                                                    Container(
                                                                  constraints:
                                                                      BoxConstraints(
                                                                    maxWidth:
                                                                        100.0,
                                                                    maxHeight:
                                                                        40.0,
                                                                  ),
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                  child: Text(
                                                                    "From camara",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontSize:
                                                                            12.0,
                                                                        letterSpacing:
                                                                            2.0,
                                                                        fontWeight:
                                                                            FontWeight.w300),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              height: 10,
                                                            ),
                                                            RaisedButton(
                                                              onPressed: () {
                                                                getImageGallery();
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              },
                                                              color: isOffline
                                                                  ? Colors
                                                                      .blueGrey
                                                                  : Colors
                                                                      .green,
                                                              shape:
                                                                  RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            80.0),
                                                              ),
                                                              child: Ink(
                                                                decoration:
                                                                    BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              80.0),
                                                                ),
                                                                child:
                                                                    Container(
                                                                  constraints:
                                                                      BoxConstraints(
                                                                    maxWidth:
                                                                        100.0,
                                                                    maxHeight:
                                                                        40.0,
                                                                  ),
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                  child: Text(
                                                                    "From galery",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontSize:
                                                                            12.0,
                                                                        letterSpacing:
                                                                            2.0,
                                                                        fontWeight:
                                                                            FontWeight.w300),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      actions: [],
                                                    );
                                                  });
                                              //getImage();
                                            },
                                            child: CircleAvatar(
                                              backgroundColor: Colors.brown,
                                              radius: 25.0,
                                              child: new Icon(
                                                Icons.camera_alt,
                                              ),
                                            )),
                                      ],
                                    ))
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 50,
                          ),
                          TextFormField(
                            onChanged: (val) {
                              setState(() {
                                name = val;
                              });
                            },
                            decoration:
                                InputDecoration(labelText: 'Pet\'s name'),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          TextFormField(
                            onChanged: (val) {
                              setState(() {
                                breed = val;
                              });
                            },
                            decoration:
                                InputDecoration(labelText: 'Pet\'s breed'),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          TextFormField(
                            onChanged: (val) {
                              setState(() {
                                age = val;
                              });
                            },
                            decoration:
                                InputDecoration(labelText: 'Pet\'s age'),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          // TextFormField(
                          //   onChanged: (val) {
                          //     setState(() {
                          //       size = val;
                          //     });
                          //   },
                          //   decoration:
                          //       InputDecoration(labelText: 'Pet\'s size'),
                          // ),
                          Row(
                            children: [
                              Text("Size:                                "),
                              DropdownButton<String>(
                                value: size,
                                icon: const Icon(Icons.arrow_downward),
                                iconSize: 24,
                                elevation: 16,
                                style: const TextStyle(color: Colors.brown),
                                underline: Container(
                                  height: 2,
                                  color: Colors.brown,
                                ),
                                onChanged: (String newValue) {
                                  setState(() {
                                    size = newValue;
                                  });
                                },
                                items: <String>[
                                  'small',
                                  'medium',
                                  'large'
                                ].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 15,
                          ),

                          Row(
                            children: <Widget>[
                              Expanded(
                                child: SizedBox(
                                  height: 200,
                                  child: ListView.builder(
                                    shrinkWrap: true,
                                    padding: const EdgeInsets.all(8.0),
                                    itemCount: defaultTags.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      var item = defaultTags[index];
                                      return CheckboxListTile(
                                        title: Text(item['name']),
                                        value: item['is_active'],
                                        onChanged: (val) {
                                          val
                                              ? petTags.add(item['name'])
                                              : petTags.remove(item['name']);
                                          setState(() {
                                            item['is_active'] = val;
                                          });
                                          print(petTags);
                                        },
                                      );
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          RaisedButton(
                            onPressed: () {
                              isOffline
                                  ? AuthyAlert.showErrorDialog(
                                      context, 'No internet conection')
                                  : createpet(context);

                              // Navigator.of(context).pushReplacement(
                              //         MaterialPageRoute(
                              //             builder: (context) => HomeScreen()));
                            },
                            color: isOffline ? Colors.blueGrey : Colors.green,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(80.0),
                            ),
                            child: Ink(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(80.0),
                              ),
                              child: Container(
                                constraints: BoxConstraints(
                                  maxWidth: 100.0,
                                  maxHeight: 40.0,
                                ),
                                alignment: Alignment.center,
                                child: Text(
                                  "Create pet",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 12.0,
                                      letterSpacing: 2.0,
                                      fontWeight: FontWeight.w300),
                                ),
                              ),
                            ),
                          ),
                        ]))),
          ],
        )));
  }
}
