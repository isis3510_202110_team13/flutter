import 'package:flutter/material.dart';
import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:pet_services_flutter/models/pet_owner.dart';
import 'package:pet_services_flutter/services/auth.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
class SettingsScreen extends StatefulWidget {
  final ThemeMode themeMode;
  final FlexSchemeData flexSchemeData;
  final Stream<ThemeMode> stream;
  SettingsScreen(
      { this.themeMode, this.stream,this.flexSchemeData});
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool status = false;


  bool autoDarkMode= false;
  final AuthServiceF _auth= AuthServiceF();

  ThemeMode theme = ThemeMode.light;

  void _updateThemeMode(ThemeMode themeMode) {
    setState(() {
      theme = themeMode;
    });
  }

  @override
  void initState(){
    getAutoDarkMode();
    widget.stream.listen((themeMode) {
      _updateThemeMode(themeMode);
    });
  }

  setAutoDarkMode(bool set) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('AutoDarkMode', set);
  }
  getAutoDarkMode() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    autoDarkMode=prefs.getBool('AutoDarkMode');
    if(autoDarkMode==null){
      autoDarkMode=false;
    }
  }

  @override
  Widget build(BuildContext context) {

    if(autoDarkMode==null){
      autoDarkMode=false;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
        backgroundColor: Colors.green[900],
      ),
      body:
      FutureBuilder(
        future: SharedPreferences.getInstance(),
    builder: (context,snapshot){
    if(snapshot.connectionState==ConnectionState.done){
    autoDarkMode = snapshot.data.getBool('AutoDarkMode');
    if(autoDarkMode==null)
    autoDarkMode=false;
    return

      Container(
          child: Column(
        children: <Widget>[
          autoDarkMode?Text(""):Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "Dark mode",
                        style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Icon(Icons.wb_sunny_outlined),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Padding(padding: EdgeInsets.only(left: 150)),
                    Switch(
                        activeColor: Colors.blue,
                        value: (Provider.of<PetOwner>(context, listen: false).theme==ThemeMode.dark),
                        onChanged: (value) {


                          if (value == true) {

                            setState(() {
                              Provider.of<PetOwner>(context, listen: false).theme=ThemeMode.dark;

                            });


                          } else {

                            setState(() {
                              Provider.of<PetOwner>(context, listen: false).theme=ThemeMode.light;

                            });
                          }

                        }),
                  ],
                ),
              ])
          ,
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    RichText(
                      text: TextSpan(
                        text: "Auto dark mode",
                        style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Icon(Icons.wb_sunny_outlined),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Padding(padding: EdgeInsets.only(left: 150)),

                    Switch(
                        activeColor: Colors.blue,
                        value: autoDarkMode,
                        onChanged: (value) {

                          setState(() {
                            autoDarkMode=value;
                            setAutoDarkMode(value);
                          });
                        }),
                  ],
                ),
              ]),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              ElevatedButton.icon(
                  onPressed: () async{
                    await _auth.signOut();
                  },
                  icon: Icon(Icons.person),
                  label: Text('Log out'))
            ],
          ),
        ],
      ));
    }
    else{return CircularProgressIndicator();}
    })
    );
  }
}
