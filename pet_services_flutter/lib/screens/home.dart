import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pet_services_flutter/models/pet_owner.dart';
import 'package:pet_services_flutter/screens/top_rated_walkers.dart';
import 'package:pet_services_flutter/services/auth.dart';
import 'package:pet_services_flutter/screens/homeScreenBody.dart';
import 'package:pet_services_flutter/screens/profileScreen.dart';
import 'package:pet_services_flutter/screens/settingsScreen.dart';
import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:intl/intl.dart';
import 'package:daylight/daylight.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';


class HomeScreen extends StatefulWidget {

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final AuthServiceF _auth= AuthServiceF();
  int _selectedIndex = 0;

  final _homeScreenBody = GlobalKey<NavigatorState>();
  final _profileScreen = GlobalKey<NavigatorState>();
  final _settingsScreen = GlobalKey<NavigatorState>();

  final _petWalkersScreen = GlobalKey<NavigatorState>();



  _checkPermissions() async {}


  String userLocation;
  DateTime sunrise;
  DateTime sunset;
  DateTime actualDate;
  DateTime morning;
  String locationMessageLat = "4.685772";
  String locationMessageLong= "-74.053082";

  bool autoDarkMode=false;
  var lat;
  var long;
  Geolocator _geolocator=Geolocator();
  Future<Position> _getCurrentLocation() async {
    return _geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best);}


  @override
  void initState() {
    _getCurrentLocation();
    //_themeBasedOnTime();
    super.initState();
  }

  void _onTap(int val, BuildContext context) {
    if (_selectedIndex == val) {
      switch (val) {
        case 0:
          _homeScreenBody.currentState.popUntil((route) => route.isFirst);
          break;
        case 1:
          _petWalkersScreen.currentState.popUntil((route) => route.isFirst);
          break;
        case 2:
          _profileScreen.currentState.popUntil((route) => route.isFirst);
          break;
        case 3:
          _settingsScreen.currentState.popUntil((route) => route.isFirst);
          break;
        default:
          _homeScreenBody.currentState.popUntil((route) => route.isFirst);
      }
    } else {
      if (mounted) {
        setState(() {
          _selectedIndex = val;
        });
      }
    }
  }

  void _themeBasedOnTime(lat, long){
    final place = DaylightLocation(lat, long);
    final placeTimeCalculator = DaylightCalculator(place);
    final dailyResults = placeTimeCalculator.calculateForDay(DateTime.now(), Zenith.official);
    sunrise= DateFormat("HH:mm:ss").parse(DateFormat("HH:mm:ss").format(dailyResults.sunrise));
    sunset= DateFormat("HH:mm:ss").parse(DateFormat("HH:mm:ss").format(dailyResults.sunset));
    actualDate = DateFormat("HH:mm:ss").parse(DateFormat('kk:mm:ss').format(DateTime.now()));
    sunrise= sunrise.subtract(Duration(hours:5));
    sunset= sunset.subtract(Duration(hours:5));
    morning=DateFormat("HH:mm:ss").parse("06:00:00");
    if (sunset.isBefore(morning)){
      sunset=DateFormat("HH:mm:ss").parse("23:59:59");
    }
    if (sunrise.isBefore(actualDate)){
      debugPrint(DateFormat("HH:mm:ss").format(sunrise));
      debugPrint("Es despues de la mañana");
    }
    if (actualDate.isBefore(sunset)){
      debugPrint(DateFormat("HH:mm:ss").format(sunset));
      debugPrint("Es antes de la noche");
    }
    debugPrint("La hora actual es: ");
    debugPrint(DateFormat('kk:mm:ss').format(DateTime.now()));
    debugPrint("El sunset es a las: ");
    debugPrint(DateFormat("HH:mm:ss").format(sunset));
    debugPrint("El sunrise es a las: ");
    debugPrint(DateFormat("HH:mm:ss").format(sunrise));
  }

  getAutoDarkMode() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    autoDarkMode=prefs.getBool('AutoDarkMode');
    if(autoDarkMode==null){
      autoDarkMode=false;
    }
  }

  Widget build(BuildContext context) {

    final petOwner= Provider.of<PetOwner>(context);
    //Provider.of<PetOwner>(context).theme=ThemeMode.light;



    StreamController<ThemeMode> _controller = StreamController<ThemeMode>();
    const FlexScheme usedFlexScheme = FlexScheme.green;
    ThemeMode themeMode = ThemeMode.light;
    ThemeMode theme;


    return

    FutureBuilder(
        future: SharedPreferences.getInstance(),
        builder: (context,snapshot){
      if(snapshot.connectionState==ConnectionState.done){
        autoDarkMode = snapshot.data.getBool('AutoDarkMode');
        if(autoDarkMode==null)
          autoDarkMode=false;

        if(autoDarkMode)

          if(lat==null){
            return FutureBuilder(
                  future: _getCurrentLocation(),
                  builder: (context,snapshot){
                    if(snapshot.connectionState==ConnectionState.done){
                      lat= snapshot.data.latitude;
                      long= snapshot.data.longitude;
                      _themeBasedOnTime(lat,long);
                      if(autoDarkMode){
                        sunrise.isBefore(actualDate) && actualDate.isBefore(sunset) ? theme=ThemeMode.light :theme=ThemeMode.dark;
                      }
                      return


                        MaterialApp(


                          themeMode: autoDarkMode?theme: (petOwner.theme==null?ThemeMode.light:petOwner.theme),

                          theme:
                          FlexColorScheme.light(scheme: usedFlexScheme).toTheme,
                          darkTheme:
                          FlexColorScheme.dark(scheme: usedFlexScheme).toTheme,

                          home: Scaffold(

                            body:IndexedStack(
                              index:_selectedIndex,
                              children:<Widget> [
                                Navigator(
                                  key: _homeScreenBody,
                                  onGenerateRoute: (route) => MaterialPageRoute(
                                    settings: route,
                                    builder: (context) => HomeScreenBody(),
                                  ),
                                ),
                                Navigator(
                                  key: _petWalkersScreen,
                                  onGenerateRoute: (route) => MaterialPageRoute(
                                    settings: route,
                                    builder: (context) => TopRatedWalkers(),
                                  ),
                                ),
                                Navigator(
                                  key: _profileScreen,
                                  onGenerateRoute: (route) => MaterialPageRoute(
                                    settings: route,
                                    builder: (context) => ProfileScreen(),
                                  ),
                                ),
                                Navigator(
                                  key: _settingsScreen,
                                  onGenerateRoute: (route) => MaterialPageRoute(
                                    settings: route,
                                    builder: (context) => SettingsScreen(
                                      // We pass it the current theme mode.
                                      themeMode: themeMode,
                                      stream: _controller.stream,
                                      // On the home page we can toggle theme mode between light and dark.

                                      flexSchemeData: FlexColor.schemes[usedFlexScheme],


                                    ),
                                  ),
                                ),
                              ],
                            ),

                            bottomNavigationBar: BottomNavigationBar(
                              type: BottomNavigationBarType.fixed,
                              backgroundColor: Colors.green[900],
                              currentIndex: _selectedIndex,
                              selectedItemColor: Colors.amber[800],
                              onTap: (val) => _onTap(val, context),
                              items: const <BottomNavigationBarItem>[
                                BottomNavigationBarItem(
                                  icon: Icon(Icons.home),
                                  label: 'Home',
                                ),
                                BottomNavigationBarItem(
                                  icon: Icon(Icons.storage),
                                  label: 'Pet walkers',
                                ),
                                BottomNavigationBarItem(
                                  icon: Icon(Icons.account_circle_sharp),
                                  label: 'Profile',
                                ),
                                BottomNavigationBarItem(
                                  icon: Icon(Icons.settings),
                                  label: 'Settings',
                                ),
                              ],
                            ),
                          ),
                        );

                    }
                    else{return CircularProgressIndicator();}
                  }
              );

          }else{
            sunrise.isBefore(actualDate) && actualDate.isBefore(sunset) ? theme=ThemeMode.light :theme=ThemeMode.dark;
            if(autoDarkMode){
              sunrise.isBefore(actualDate) && actualDate.isBefore(sunset) ? theme=ThemeMode.light :theme=ThemeMode.dark;
            }
            return  MaterialApp(


              themeMode: autoDarkMode?theme: (petOwner.theme==null?ThemeMode.light:petOwner.theme),

              theme:
              FlexColorScheme.light(scheme: usedFlexScheme).toTheme,
              darkTheme:
              FlexColorScheme.dark(scheme: usedFlexScheme).toTheme,

              home: Scaffold(

                body:IndexedStack(
                  index:_selectedIndex,
                  children:<Widget> [
                    Navigator(
                      key: _homeScreenBody,
                      onGenerateRoute: (route) => MaterialPageRoute(
                        settings: route,
                        builder: (context) => HomeScreenBody(),
                      ),
                    ),
                    Navigator(
                      key: _petWalkersScreen,
                      onGenerateRoute: (route) => MaterialPageRoute(
                        settings: route,
                        builder: (context) => TopRatedWalkers(),
                      ),
                    ),
                    Navigator(
                      key: _profileScreen,
                      onGenerateRoute: (route) => MaterialPageRoute(
                        settings: route,
                        builder: (context) => ProfileScreen(),
                      ),
                    ),
                    Navigator(
                      key: _settingsScreen,
                      onGenerateRoute: (route) => MaterialPageRoute(
                        settings: route,
                        builder: (context) => SettingsScreen(
                          // We pass it the current theme mode.
                          themeMode: themeMode,
                          stream: _controller.stream,
                          // On the home page we can toggle theme mode between light and dark.

                          flexSchemeData: FlexColor.schemes[usedFlexScheme],


                        ),
                      ),
                    ),
                  ],
                ),

                bottomNavigationBar: BottomNavigationBar(
                  type: BottomNavigationBarType.fixed,
                  backgroundColor: Colors.green[900],
                  currentIndex: _selectedIndex,
                  selectedItemColor: Colors.amber[800],
                  onTap: (val) => _onTap(val, context),
                  items: const <BottomNavigationBarItem>[
                    BottomNavigationBarItem(
                      icon: Icon(Icons.home),
                      label: 'Home',
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(Icons.storage),
                      label: 'Pet walkers',
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(Icons.account_circle_sharp),
                      label: 'Profile',
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(Icons.settings),
                      label: 'Settings',
                    ),
                  ],
                ),
              ),
            );
          }

        else
          return MaterialApp(


            themeMode: autoDarkMode?theme: (petOwner.theme==null?ThemeMode.light:petOwner.theme),

            theme:
            FlexColorScheme.light(scheme: usedFlexScheme).toTheme,
            darkTheme:
            FlexColorScheme.dark(scheme: usedFlexScheme).toTheme,

            home: Scaffold(

              body:IndexedStack(
                index:_selectedIndex,
                children:<Widget> [
                  Navigator(
                    key: _homeScreenBody,
                    onGenerateRoute: (route) => MaterialPageRoute(
                      settings: route,
                      builder: (context) => HomeScreenBody(),
                    ),
                  ),
                  Navigator(
                    key: _petWalkersScreen,
                    onGenerateRoute: (route) => MaterialPageRoute(
                      settings: route,
                      builder: (context) => TopRatedWalkers(),
                    ),
                  ),
                  Navigator(
                    key: _profileScreen,
                    onGenerateRoute: (route) => MaterialPageRoute(
                      settings: route,
                      builder: (context) => ProfileScreen(),
                    ),
                  ),
                  Navigator(
                    key: _settingsScreen,
                    onGenerateRoute: (route) => MaterialPageRoute(
                      settings: route,
                      builder: (context) => SettingsScreen(
                        // We pass it the current theme mode.
                        themeMode: themeMode,
                        stream: _controller.stream,
                        // On the home page we can toggle theme mode between light and dark.

                        flexSchemeData: FlexColor.schemes[usedFlexScheme],


                      ),
                    ),
                  ),
                ],
              ),

              bottomNavigationBar: BottomNavigationBar(
                type: BottomNavigationBarType.fixed,
                backgroundColor: Colors.green[900],
                currentIndex: _selectedIndex,
                selectedItemColor: Colors.amber[800],
                onTap: (val) => _onTap(val, context),
                items: const <BottomNavigationBarItem>[
                  BottomNavigationBarItem(
                    icon: Icon(Icons.home),
                    label: 'Home',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.storage),
                    label: 'Pet walkers',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.account_circle_sharp),
                    label: 'Profile',
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.settings),
                    label: 'Settings',
                  ),
                ],
              ),
            ),
          );
      }
      else{return CircularProgressIndicator();}
    });



}




}