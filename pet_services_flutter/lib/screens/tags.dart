import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pet_services_flutter/screens/home.dart';
import 'dart:convert';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
class TagsScreen extends StatefulWidget {
  @override
  _TagsScreen createState() => _TagsScreen();
}

class _TagsScreen extends State<TagsScreen> {
  List data;
  List usersData;
  getTags() async {
    var uri = Uri.parse('http://192.168.1.38:4000/dogwalkers/matched/user?username=j.tambo');
    http.Response response = await http.get(uri);
    debugPrint(response.body);
    data = jsonDecode(response.body);

    setState(() {
      usersData = data;
    });
  }
  @override
  void initState() {

    super.initState();
    getTags();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Match walkers!'),
          leading: IconButton(
            icon: Icon(
              FontAwesomeIcons.solidArrowAltCircleLeft,
              size: 45.0,
              color: Colors.green,
            ),
            onPressed: () => Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => HomeScreen())),
          ),
          backgroundColor: Colors.green[900],
          actions: <Widget>[
            Builder(builder: (BuildContext context) {
//5
              return FlatButton(
                child: const Text('Sign out'),
                textColor: Theme.of(context).buttonColor,
                onPressed: () => Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => HomeScreen())),
              );
            })
          ],
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                itemCount: usersData == null ? 0 : usersData.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    child: Row(
                      children: <Widget>[


                        Padding(
                            padding: const EdgeInsets.all(30.0),
                            child: Text(
                              "${usersData[index]["username"]}",
                              style: TextStyle(
                                fontSize: 20.0,
                                fontWeight: FontWeight.w500,
                              ),
                            )),
                        Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Text(
                              "${usersData[index]["tags"]}",
                              style: TextStyle(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.green[900]),
                            )),
                      ],
                    ),
                  );
                },
              ),
            ),

            Text(' '),
            Text(' '),
          ],
        ));

  }
}
