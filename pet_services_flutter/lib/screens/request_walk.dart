import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:pet_services_flutter/screens/no_conection_bar.dart';
import 'package:pet_services_flutter/services/auth.dart';
import 'package:pet_services_flutter/services/connection_status_singleton.dart';
import 'package:pet_services_flutter/screens/pet_walker_detail.dart';
import 'package:pet_services_flutter/services/pet_walkers_service.dart';
import 'package:pet_services_flutter/services/storage.dart';

import 'homeScreenBody.dart';

class RequestWalkScreen extends StatefulWidget {
  RequestWalkScreen({Key key, @required this.storage}) : super(key: key);

  @override
  _RequestWalkScreenState createState() => _RequestWalkScreenState();
  final Storage storage;
}

class _RequestWalkScreenState extends State<RequestWalkScreen> {
  StreamSubscription _connectionChangeStream;
  
  bool isOffline = false;
  var petWalkers = [];

  void connectionChanged(dynamic hasConnection) {
    setState(() {
      isOffline = !hasConnection;
    });
  }

  @override
  void initState() {
    super.initState();

    ConnectionStatusSingleton connectionStatus =
        ConnectionStatusSingleton.getInstance();
    _connectionChangeStream =
        connectionStatus.connectionChange.listen(connectionChanged);
    // PetWalkersService().getTopPetWalkers().then((Response response) {
    //   petWalkers = jsonDecode(response.body);
    //
    //   debugPrint("${petWalkers}");
    // });
  }

  final AuthServiceF _auth = AuthServiceF();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Top rated walkers!'),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_outlined,
              size: 45.0,
              color: Colors.white,
            ),
            onPressed: () => Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => HomeScreenBody())),
          ),
          backgroundColor: Colors.green[900],
        ),
        body:

        Column(
          children: <Widget>[
            isOffline
                ? NoConectionBar()
                : Container(),
            isOffline?
            FutureBuilder<File>(
                future: PetWalkersService().getTopPetWalkersOffline('',false),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    debugPrint("${snapshot}");
                    //petWalkers = jsonDecode(snapshot.data.body);
                    return Expanded(
                      child: ListView.builder(
                        itemCount: petWalkers == null ? 0 : petWalkers.length,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () => Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                    builder: (context) => PetWalkerDetail(
                                        id: petWalkers[index]['uid']))),
                            child: Card(
                              child: Row(
                                children: <Widget>[
                                  petWalkers[index]['photoURL'] != ''
                                      ? Image.network(
                                      petWalkers[index]['photoURL'],
                                      height: 100,
                                      width: 100)
                                      : Image.asset('assets/dogWalker.png',
                                      height: 100, width: 100),
                                  Padding(
                                      padding: const EdgeInsets.all(30.0),
                                      child: Text(
                                        "${petWalkers[index]["displayName"]}",
                                        style: TextStyle(
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      )),
                                  Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: Text(
                                        "${petWalkers[index]["avgRating"]}",
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.green[900]),
                                      )),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  } else {
                    return CircularProgressIndicator();
                  }
                })

                :
            FutureBuilder(
                future: PetWalkersService().getTopPetWalkers('',false),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    petWalkers = jsonDecode(snapshot.data.body);
                    return Expanded(
                      child: ListView.builder(
                        itemCount: petWalkers == null ? 0 : petWalkers.length,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () => Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                    builder: (context) => PetWalkerDetail(
                                        id: petWalkers[index]['uid']))),
                            child: Card(
                              child: Row(
                                children: <Widget>[
                                  petWalkers[index]['photoURL'] != ''
                                      ? Image.network(
                                      petWalkers[index]['photoURL'],
                                      height: 100,
                                      width: 100)
                                      : Image.asset('assets/dogWalker.png',
                                      height: 100, width: 100),
                                  Padding(
                                      padding: const EdgeInsets.all(30.0),
                                      child: Text(
                                        "${petWalkers[index]["displayName"]}",
                                        style: TextStyle(
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      )),
                                  Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: Text(
                                        "${petWalkers[index]["avgRating"]}",
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.green[900]),
                                      )),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  } else {
                    return CircularProgressIndicator();
                  }
                })
          ],
        )

    );
  }
}
