
import 'package:flutter/material.dart';
import 'package:pet_services_flutter/screens/home.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pet_services_flutter/services/connection_status_singleton.dart';
import 'dart:async';

class PetOwnerDetail extends StatefulWidget {
  final String id;
  final String name;
  final String email;
  final String mobile;
  final String password;
  final String photoURL;

  PetOwnerDetail({this.id, this.name, this.email, this.mobile, this.password, this.photoURL});

  @override
  _PetOwnerDetailState createState() => _PetOwnerDetailState();
}

class _PetOwnerDetailState extends State<PetOwnerDetail> {
  StreamSubscription _connectionChangeStream;
  bool isOffline = false;
  var petOwners = [];
  var petOwner;

  void connectionChanged(dynamic hasConnection) {
    setState(() {
      isOffline = !hasConnection;
    });
  }

  @override
  void initState() {
    super.initState();

    ConnectionStatusSingleton connectionStatus =
    ConnectionStatusSingleton.getInstance();
    _connectionChangeStream =
        connectionStatus.connectionChange.listen(connectionChanged);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Detail ' + widget.id),
          leading: IconButton(
            icon: Icon(
              FontAwesomeIcons.solidArrowAltCircleLeft,
              size: 45.0,
              color: Colors.green,
            ),
            onPressed: () => Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => HomeScreen())),
          ),
          backgroundColor: Colors.green[900],
          actions: <Widget>[
            Builder(builder: (BuildContext context) {
//5
              return FlatButton(
                child: const Text('Sign out'),
                textColor: Theme.of(context).buttonColor,
                onPressed: () => Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => HomeScreen())),
              );
            })
          ],
        ),
        body: Column(
          children: <Widget>[
            isOffline
                ? Container(
              color: Colors.brown,
              width: double.infinity,
              height: 30,
              child: Padding(
                  padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                  child:
                  Text('No internet conection, showing previous data',
                      style: TextStyle(
                        backgroundColor: Colors.brown,
                        color: Colors.white,
                      ))),
            )
                : Container(),
          ],
        ));
  }
}

