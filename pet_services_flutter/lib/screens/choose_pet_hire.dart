import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:pet_services_flutter/models/pet_owner.dart';
import 'package:pet_services_flutter/screens/rate_walk.dart';
import 'package:pet_services_flutter/screens/request_walk.dart';
import 'package:pet_services_flutter/services/auth.dart';
import 'package:pet_services_flutter/services/connection_status_singleton.dart';
import 'package:pet_services_flutter/services/pet_owners_service.dart';
import 'package:pet_services_flutter/services/walk_service.dart';
import 'package:provider/provider.dart';

class HireChoosePet extends StatefulWidget {
  final String walkerId;
  HireChoosePet({this.walkerId});
  @override
  _HireChoosePetState createState() => _HireChoosePetState();

}

class _HireChoosePetState extends State<HireChoosePet> {
  
  final AuthServiceF _auth = AuthServiceF();
  
  var pets = [];
  String uid;
  String _selectedPet; // Option 2
  List<String> petsName = [];
  final _formKey = GlobalKey<FormState>();
  bool _autovalidate = false;
  void fetchPets() async {
    _auth.petOwner.first.then((value) {
      setState(() {
        uid = value.uid;
      });
      PetOwnersService().getPets(uid).then((value2) {
        setState(() {
          pets = jsonDecode(value2.body);
        });
      });
    });
  }

  StreamSubscription _connectionChangeStream;
  bool isOffline = true;

  void connectionChanged(dynamic hasConnection) {
    setState(() {
      isOffline = !hasConnection;
    });
  }

  @override
  void initState() {
    ConnectionStatusSingleton connectionStatus =
        ConnectionStatusSingleton.getInstance();
    fetchPets();
    ConnectionStatusSingleton.getInstance().checkConnection().then((value) => {
          setState(() {
            isOffline = !value;
          })
        });
    _connectionChangeStream =
        connectionStatus.connectionChange.listen(connectionChanged);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String _chosenValue;
    print(petsName);
    final petOwner = Provider.of<PetOwner>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Choose pet'),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            size: 45.0,
            color: Colors.white,
          ),
          onPressed: () => Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => RequestWalkScreen())),
        ),
        backgroundColor: Colors.green[900],
      ),
      body: Form(
        key: _formKey,
        autovalidate: _autovalidate,
        child: Column(children: <Widget>[
          Expanded(
            child: DropdownButtonFormField(
              hint: Text(
                  'Which pet you want to walk?'), // Not necessary for Option 1
              value: _selectedPet,
              validator: (value) => value == null ? 'field required' : null,
              onChanged: (newValue) {
                setState(() {
                  _selectedPet = newValue;
                });
              },

              items: pets.map((pet) {
                return DropdownMenuItem(
                  child: new Text(pet["name"]),
                  value: pet["name"],
                );
              }).toList(),
            ),
          ),
          ElevatedButton(
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  bool found = false;
                  int id = -1;
                  for (int i = 0; i < pets.length && !found; i++) {
                    if (pets[i]["name"] == _selectedPet) {
                      found = true;
                      id = pets[i]["id"];
                    }
                  }
                  var walk = new Map<String, dynamic>();
                  walk["PetWalkerUid"] = widget.walkerId;
                  walk["PetId"] = id;

                  WalksService().postWalk(walk).then((value) =>
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => RateWalk()))); 
                }
              },
              child: Text('Start Walk!'),
              style: ElevatedButton.styleFrom(
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0),
                  ),
                  primary: Colors.green)),
        ]),
      ),
    );
  }
}
