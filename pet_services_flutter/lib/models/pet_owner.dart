import 'package:flutter/material.dart';

class PetOwner {

  final String uid;
  final String displayName;
  final dynamic photoUrl;
  final String coordType;
  final double latitude;
  final double longitude;
  ThemeMode theme=ThemeMode.light;

  PetOwner( {
     this.uid,
     this.displayName,
    this.photoUrl,
    this.coordType,
    this.latitude,
    this.longitude,
    this.theme
  });

  factory PetOwner.fromJson(Map<String, dynamic> json){
    return PetOwner(
        uid: json['uid'] as String,
        displayName: json['displayName'],
        photoUrl:json['photoUrl'],
        coordType: json['currentLocation'].type,
        latitude: json['currentLocation'].coordinates[0],
        longitude: json['currentLocation'].coordinates[1]
    );
  }
  Map<String, dynamic> toJson() => {
    'uid': uid,
    'displayName': displayName,
    'photoUrl':photoUrl,
    'currentLocation': {
      'type': coordType,
      'coordinates': [latitude, longitude]
    }
  };
}
