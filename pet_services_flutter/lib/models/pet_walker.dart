import 'dart:convert';

   List<PetWalker> petWalkerFromJson(String str){
     print(str);
     List<PetWalker>.from(json.decode(str).map((x)=>PetWalker.fromJson(x)));
   }
      
  
  String petWalkerToJson(List<PetWalker> data)=>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class PetWalker{
  //Do we have to include everything in this model?
  int uid;
   String displayName;
   String photoURL;
   String description;
   double avgRating; 
   PetWalker({
     this.uid,
     this.displayName,
     this.photoURL,
     this.description,
     this.avgRating
   });

   factory PetWalker.fromJson(Map<String,dynamic> json)=> PetWalker(
    uid: json["uid"],
    displayName: json["displayName"],
    photoURL: json["photoURL"],
    description: json["description"],
    avgRating: json["avgRating"]
   );

   Map<String,dynamic> toJson()=>{
     "uid": uid,
     "displayName":displayName,
     "photoURL": photoURL,
     "description": description,
     "avgRating": avgRating
   };

}