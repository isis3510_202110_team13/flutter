import 'dart:convert';

List<Pet> petWalkerFromJson(String str) {
  print(str);
  List<Pet>.from(json.decode(str).map((x) => Pet.fromJson(x)));
}

String petWalkerToJson(List<Pet> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Pet {
  String name;
  String breed;
  int age;
  String size;
  String photoURL = "";
  List<Map<String, String>> tags;

  Pet({this.name, this.breed, this.age, this.size, this.tags, this.photoURL});

  factory Pet.fromJson(Map<String, dynamic> json) => Pet(
      name: json["name"],
      breed: json["breed"],
      age: json["age"],
      size: json["size"],
      photoURL: json["photoURL"]);

  Map<String, dynamic> toJson() => {
        "name": name,
        "breed": breed,
        "age": age,
        "size": size,
        "photoURL": photoURL
      };
}
